// swift-tools-version: 5.8
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "ThresholdSecurity",
    platforms: [
        .iOS(.v16),
        .macOS(.v13)
    ],
    products: [
        .library(
            name: "ThresholdSecurity",
            targets: ["ThresholdSecurityData", "ThresholdSecurityDomain"]
        ),
        .library(
            name: "ThresholdSecurityData",
            targets: ["ThresholdSecurityData"]
        ),
        .library(
            name: "ThresholdSecurityDomain",
            targets: ["ThresholdSecurityDomain"]
        )
    ],
    dependencies: [
        .package(
            url: "https://github.com/Liftric/DIKit.git",
            exact: "1.6.1"
        ),
        .package(
            url: "https://gitlab.com/Blockdaemon/wallet-sdk/ios/Extensions",
            exact: "1.0.0"
        ),
        .package(
            url: "https://gitlab.com/Blockdaemon/wallet-sdk/ios/ToolKit",
            exact: "1.0.0"
        ),
        .package(
            url: "https://gitlab.com/Blockdaemon/wallet-sdk/ios/NetworkKit",
            exact: "1.0.0"
        ),
        .package(
            url: "https://gitlab.com/Blockdaemon/wallet-sdk/ios/Errors",
            exact: "1.0.0"
        ),
        .package(
            url: "https://gitlab.com/Blockdaemon/wallet-sdk/ios/FeatureAuthentication",
            exact: "1.0.0"
        ),
        .package(
            url: "https://gitlab.com/Blockdaemon/wallet-sdk/ios/CommonCryptoKit",
            exact: "1.0.0"
        )
    ],
    targets: [
        .binaryTarget(
            name: "Tsm",
            path: "Sources/Tsm.xcframework"
        ),
        .target(
            name: "ThresholdSecurityDomain",
            dependencies: [
                "NetworkKit",
                "CommonCryptoKit",
                "Extensions",
                "Errors",
                "ToolKit",
                .product(name: "DIKit", package: "DIKit")
            ]
        ),
        .target(
            name: "ThresholdSecurityData",
            dependencies: [
                .target(name: "Tsm"),
                .target(name: "ThresholdSecurityDomain"),
                "FeatureAuthentication",
                "CommonCryptoKit",
                "Extensions",
                "NetworkKit",
                "Errors",
                "ToolKit",
                .product(name: "DIKit", package: "DIKit"),
            ]
        ),
        .testTarget(
            name: "ThresholdSecurityTests",
            dependencies: [
                .target(name: "Tsm"),
                .target(name: "ThresholdSecurityDomain"),
                .target(name: "ThresholdSecurityData"),
                "Errors"
            ]
        )
    ]
)
