import XCTest
import Combine
@testable import CommonCryptoKit
@testable import ThresholdSecurityDomain
@testable import ThresholdSecurityData

class KeyIdDirectoryTests: XCTestCase {
    var keyIdDirectory: KeyIdDirectoryAPI!
    var mockUserDefaults: MockUserDefaults!
    var cancellables: Set<AnyCancellable>!
    
    override func setUp() {
        super.setUp()
        mockUserDefaults = MockUserDefaults()
        keyIdDirectory = KeyIdDirectory(defaults: mockUserDefaults)
        cancellables = []
    }
    
    override func tearDown() {
        keyIdDirectory = nil
        mockUserDefaults = nil
        cancellables = nil
        super.tearDown()
    }
    
    func testSubscriptGetSet() {
        let userId = "testUser"
        let curve = Curve.secp256k1
        let keyId = KeyId(curve: curve, identifier: "key123")
        
        keyIdDirectory[userId, curve] = keyId
        XCTAssertEqual(keyIdDirectory[userId, curve], keyId)
    }
    
    func testAddKeyIdToUserIdSuccess() {
        let userId = "testUser"
        let keyId = KeyId(curve: .secp256k1, identifier: "key123")
        
        let expectation = XCTestExpectation(description: "Add KeyId to UserId succeeds")
        
        keyIdDirectory.addKeyIdToUserId(userId, keyId: keyId)
            .sink(receiveCompletion: { completion in
                switch completion {
                case .finished:
                    expectation.fulfill()
                case .failure(let error):
                    print("\(error)")
                    XCTFail("Add KeyId to UserId should succeed")
                }
            }, receiveValue: { _ in })
            .store(in: &cancellables)
        
        wait(for: [expectation], timeout: 1.0)
        // Verify if the keyId was added
        XCTAssertEqual(keyIdDirectory[userId, keyId.curve], keyId)
    }
    
    func testDoesUserIdHaveKeyIdForAllCurveTypes() {
        let userId = "testUser"
        let keyId1 = KeyId(curve: .secp256k1, identifier: "key123")
        let keyId2 = KeyId(curve: .ed25519, identifier: "key456")

        let expectation = XCTestExpectation(description: "Receive updated value for user key IDs")
        // One for initial value, another for after adding keyIds
        expectation.expectedFulfillmentCount = 2
        
        keyIdDirectory.doesUserIdHaveKeyIdForAllCurveTypes(userId)
            .sink(receiveValue: { hasAllKeyIds in
                if hasAllKeyIds {
                    XCTAssertTrue(hasAllKeyIds)
                }
                expectation.fulfill()
            })
            .store(in: &cancellables)
        
        keyIdDirectory.addKeyIdToUserId(userId, keyId: keyId1)
            .sink(receiveCompletion: { _ in }, receiveValue: { _ in })
            .store(in: &cancellables)
        keyIdDirectory.addKeyIdToUserId(userId, keyId: keyId2)
            .sink(receiveCompletion: { _ in }, receiveValue: { _ in })
            .store(in: &cancellables)
        
        wait(for: [expectation], timeout: 1.0)
    }
    
    func testInitialSubscriptionValue() {
        let userId = "testUser"
        let expectation = XCTestExpectation(description: "Receive initial subscription value")
        
        keyIdDirectory.doesUserIdHaveKeyIdForAllCurveTypes(userId)
            .sink(receiveValue: { hasAllKeyIds in
                // Assuming initially there are no keyIds for the user
                XCTAssertFalse(hasAllKeyIds)
                expectation.fulfill()
            })
            .store(in: &cancellables)
        
        wait(for: [expectation], timeout: 1.0)
    }
    
    func testValueChangeNotification() {
        let userId = "testUser"
        let keyId = KeyId(curve: .secp256k1, identifier: "key123")
        let expectation = XCTestExpectation(description: "Receive notification on value change")
        // Initial value and after change
        expectation.expectedFulfillmentCount = 2
        
        keyIdDirectory.doesUserIdHaveKeyIdForAllCurveTypes(userId)
            .sink(receiveValue: { hasAllKeyIds in
                // This should be called twice
                expectation.fulfill()
            })
            .store(in: &cancellables)
        
        // Trigger a change
        keyIdDirectory.addKeyIdToUserId(userId, keyId: keyId)
            .sink(receiveCompletion: { _ in }, receiveValue: { _ in })
            .store(in: &cancellables)
        
        wait(for: [expectation], timeout: 1.0)
    }
    
    func testSerializationAndDeserializationOfKeyUserIdWrapper() {
        // 1. Create a KeyUserIdWrapper object
        let userId = "testUser"
        let keyId1 = KeyId(curve: .secp256k1, identifier: "key123")
        let keyId2 = KeyId(curve: .ed25519, identifier: "key456")
        let wrapper = KeyUserIdWrapper(userId: userId, keyIds: [keyId1, keyId2])
        
        // 2. Serialize it to Data
        let data: Data
        do {
            data = try NSKeyedArchiver.archivedData(withRootObject: wrapper, requiringSecureCoding: true)
        } catch {
            XCTFail("Failed to serialize KeyUserIdWrapper: \(error)")
            return
        }
        
        // 3. Deserialize it back to a KeyUserIdWrapper
        let decodedWrapper: KeyUserIdWrapper
        do {
            decodedWrapper = try NSKeyedUnarchiver.unarchivedObject(ofClass: KeyUserIdWrapper.self, from: data)!
        } catch {
            XCTFail("Failed to deserialize KeyUserIdWrapper: \(error)")
            return
        }
        
        // 4. Verify that the deserialized object matches the original
        XCTAssertEqual(decodedWrapper.userId, wrapper.userId)
        XCTAssertEqual(decodedWrapper.keyIds.count, wrapper.keyIds.count)
        XCTAssertEqual(decodedWrapper.keyIds[0].identifier, keyId1.identifier)
        XCTAssertEqual(decodedWrapper.keyIds[1].identifier, keyId2.identifier)
    }
    
    func testSerializationWithEmptyKeyIds() {
        let userId = "testUser"
        let wrapper = KeyUserIdWrapper(userId: userId, keyIds: [])

        let data = try! NSKeyedArchiver.archivedData(withRootObject: wrapper, requiringSecureCoding: true)
        let decodedWrapper = try! NSKeyedUnarchiver.unarchivedObject(ofClass: KeyUserIdWrapper.self, from: data)!

        XCTAssertEqual(decodedWrapper.userId, wrapper.userId)
        XCTAssertTrue(decodedWrapper.keyIds.isEmpty)
    }
    
    func testSerializationWithLargeNumberOfKeyIds() {
        let userId = "testUser"
        let keyIds = (1...1000).map { KeyId(curve: .secp256k1, identifier: "key\($0)") }
        let wrapper = KeyUserIdWrapper(userId: userId, keyIds: keyIds)

        let data = try! NSKeyedArchiver.archivedData(withRootObject: wrapper, requiringSecureCoding: true)
        let decodedWrapper = try! NSKeyedUnarchiver.unarchivedObject(ofClass: KeyUserIdWrapper.self, from: data)!

        XCTAssertEqual(decodedWrapper.userId, wrapper.userId)
        XCTAssertEqual(decodedWrapper.keyIds.count, keyIds.count)
    }
    
    func testSerializationWithDifferentCurveTypes() {
        let userId = "testUser"
        let keyId1 = KeyId(curve: .secp256k1, identifier: "key123")
        let keyId2 = KeyId(curve: .ed25519, identifier: "key456")
        // Add more curves as necessary
        let wrapper = KeyUserIdWrapper(userId: userId, keyIds: [keyId1, keyId2])

        let data = try! NSKeyedArchiver.archivedData(withRootObject: wrapper, requiringSecureCoding: true)
        let decodedWrapper = try! NSKeyedUnarchiver.unarchivedObject(ofClass: KeyUserIdWrapper.self, from: data)!

        XCTAssertEqual(decodedWrapper.userId, wrapper.userId)
        XCTAssertEqual(decodedWrapper.keyIds.count, 2)
        XCTAssertEqual(decodedWrapper.keyIds[0].curve, keyId1.curve)
        XCTAssertEqual(decodedWrapper.keyIds[1].curve, keyId2.curve)
    }
}
