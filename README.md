# ThresholdSecurity

A package that wraps the TSM framework. 

This package includes `ThresholdSecurityData` and `ThresholdSecurityDomain`

### ThresholdSecurityData

Wraps the TSM framework. Only this library should access TSM. No APIs here should be exposed to the SDK target. 

### ThresholdSecurityDomain

Calls into `ThresholdSecurityData`. This includes APIs that are exposed to the SDK target.  
