// Copyright © Blockdaemon All rights reserved.

import DIKit
import ThresholdSecurityDomain

extension DependencyContainer {
    
    // MARK: - ThresholdSecurityData
    
    public static var thresholdSecurityData = module {
        
        factory { APIClient() as ThresholdSecuritySessionClientAPI }
        
        factory { APIClient() as RecoveryClientAPI }
        
        factory { APIClient() as MessageSigningClientAPI }
        
        factory { APIClient() as PresignatureClientAPI }
        
        factory { APIClient() as TSMConfigurationClientAPI }
        
        factory { APIClient() as KeyGenWalletAttributeClientAPI }
        
        factory(tag: DIKitContext.eddsa) { () -> KeyGenRepositoryAPI in
            EDDSAKeyGenRepository() as KeyGenRepositoryAPI
        }

        factory(tag: DIKitContext.ecdsa) { () -> KeyGenRepositoryAPI in
            ECDSAKeyGenRepository() as KeyGenRepositoryAPI
        }
        
        factory { TSMGlobalRepository() as TSMGlobalRepositoryAPI }
        
        factory { KeyGenWalletAttributeRepository() as KeyGenWalletAttributeRepositoryAPI }
        
        factory(tag: DIKitContext.eddsa) { () -> RecoveryRepositoryAPI in
            EDDSARecoveryRepository() as RecoveryRepositoryAPI
        }

        factory(tag: DIKitContext.ecdsa) { () -> RecoveryRepositoryAPI in
            ECDSARecoveryRepository() as RecoveryRepositoryAPI
        }
        
        factory { ERSKeyPairRepository() as ERSKeyPairRepositoryAPI }
        
        factory { CloudKitRepository() as CloudKitRepositoryAPI }
        
        factory { PrivateKeyRepository() as PrivateKeyRepositoryAPI }
        
        factory { SessionRepository() as SessionRepositoryAPI }
        
        factory { PresignatureRepository() as PresignatureRepositoryAPI }
        
        factory { TSMConfigurationRepository() as TSMConfigurationRepositoryAPI }
        
        factory { MessageSigningProvider() as MessageSigningProviderAPI }
        
        single { TSMClientProvider() as TSMClientProviderAPI }
    }
}
