// Copyright © Blockdaemon All rights reserved.

import Combine
import DIKit
import Errors
import Extensions
import FeatureAuthenticationDomain

enum TSMConfigurationRepositoryError: Error {
    case unknown
    case emptyUserId
    case network(NetworkError)
}

protocol TSMConfigurationRepositoryAPI {
    var configuration: AnyPublisher<Config, TSMConfigurationRepositoryError> { get }
}

final class TSMConfigurationRepository: TSMConfigurationRepositoryAPI {
    
    private var cache: Config?
    
    var configuration: AnyPublisher<Config, TSMConfigurationRepositoryError> {
        if let cachedValue = cache {
            return .just(cachedValue)
        } else {
            return fetchConfiguration()
                .share()
                .eraseToAnyPublisher()
        }
    }
    
    private let client: TSMConfigurationClientAPI
    private let userRepository: BlockdaemonUserRepositoryAPI
    
    init(
        client: TSMConfigurationClientAPI = resolve(),
        userRepository: BlockdaemonUserRepositoryAPI = resolve()
    ) {
        self.client = client
        self.userRepository = userRepository
    }
    
    private func fetchConfiguration() -> AnyPublisher<Config, TSMConfigurationRepositoryError> {
        userRepository
            .userId
            .flatMap { [client] userId -> AnyPublisher<Config, TSMConfigurationRepositoryError> in
                guard let id = userId else {
                    return .failure(TSMConfigurationRepositoryError.emptyUserId)
                }
                return client
                    .fetchNodeConfigurationWithUserId(id)
                    .mapError(TSMConfigurationRepositoryError.network)
                    .eraseToAnyPublisher()
            }
            .handleEvents(receiveOutput: { [weak self] configuration in
                self?.cache = configuration
            })
            .eraseToAnyPublisher()
    }
}
