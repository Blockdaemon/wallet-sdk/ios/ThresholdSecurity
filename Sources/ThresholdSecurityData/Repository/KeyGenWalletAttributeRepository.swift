// Copyright © Blockdaemon All rights reserved.

import Combine
import DIKit
import Errors
import ThresholdSecurityDomain

final class KeyGenWalletAttributeRepository: KeyGenWalletAttributeRepositoryAPI {
    
    private let client: KeyGenWalletAttributeClientAPI
    
    init(client: KeyGenWalletAttributeClientAPI = resolve()) {
        self.client = client
    }
    
    func createWalletAttributeForKeyId(
        _ keyId: String,
        userId: String
    ) -> AnyPublisher<Void, NetworkError> {
        client
            .createWalletAttributeForKeyId(keyId, userId: userId)
            .eraseToAnyPublisher()
    }
}
