// Copyright © Blockdaemon All rights reserved.

import CommonCryptoKit
import Combine
import DIKit
import Errors
import Tsm
import ThresholdSecurityDomain
import ToolKit

final class PresignatureRepository: PresignatureRepositoryAPI {
    
    private let client: PresignatureClientAPI
    private let provider: TSMClientProviderAPI
    
    init(
        client: PresignatureClientAPI = resolve(),
        provider: TSMClientProviderAPI = resolve()
    ) {
        self.client = client
        self.provider = provider
    }
    
    func generatePresignatureSessionIdForUserId(
        _ userId: String,
        publicKey: String,
        keyId: String,
        curveType: Curve
    ) -> AnyPublisher<String, PresignatureRepositoryError> {
        client
            .generatePresignatureSessionIDForUserId(
                userId,
                publicKey: publicKey,
                count: 1,
                keyId: keyId,
                curve: curveType
            )
            .mapError(PresignatureRepositoryError.network)
            .flatMap { sessionId in
                self.generatePresignatureWithSessionId(
                    sessionId,
                    keyId: keyId,
                    curveType: curveType
                )
            }
            .eraseToAnyPublisher()
    }
    
    private func generatePresignatureWithSessionId(
        _ sessionId: String,
        keyId: String,
        curveType: Curve
    ) -> AnyPublisher<String, PresignatureRepositoryError> {
        switch curveType {
        case .ed25519:
            return provider
                .eddsa
                .mapError(PresignatureRepositoryError.tsm)
                .flatMap { tsm -> AnyPublisher<String, PresignatureRepositoryError> in
                    var err: NSError?
                    let sessionId = tsm.presigGen(
                        withSessionID: sessionId,
                        keyID: keyId,
                        count: 1,
                        error: &err
                    )
                    if let error = err {
                        Logger.shared.error("Error generating key id: \(String(describing: err))")
                        return .failure(PresignatureRepositoryError.tsm(error))
                    }
                    return .just(sessionId)
                }
                .eraseToAnyPublisher()
        case .secp256k1:
            return provider
                .ecdsa
                .mapError(PresignatureRepositoryError.tsm)
                .flatMap { tsm -> AnyPublisher<String, PresignatureRepositoryError> in
                    var err: NSError?
                    let sessionId = tsm.presigGen(
                        withSessionID: sessionId,
                        keyID: keyId,
                        count: 1,
                        error: &err
                    )
                    if let error = err {
                        Logger.shared.error("Error generating key id: \(String(describing: err))")
                        return .failure(PresignatureRepositoryError.tsm(error))
                    }
                    return .just(sessionId)
                }
                .eraseToAnyPublisher()
        }
    }
}
