// Copyright © Blockdaemon All rights reserved.

import Combine
import CommonCryptoKit
import DIKit
import Errors
import ThresholdSecurityDomain
import Tsm

final class SessionRepository: SessionRepositoryAPI {
    
    private let client: ThresholdSecuritySessionClientAPI
    private let provider: TSMClientProviderAPI
    
    init(
        
        client: ThresholdSecuritySessionClientAPI = resolve(),
        provider: TSMClientProviderAPI = resolve()
    ) {
        self.client = client
        self.provider = provider
    }
    
    func generateKeygenSessionIdForUserId(
        _ userId: String,
        curve: Curve
    ) -> AnyPublisher<String, SessionRepositoryError> {
        fetchTenantPublicKeyForCurveType(curve)
            .flatMap { [client] publicKey in
                client
                    .generateKeygenSessionIDForUserId(
                        userId,
                        publicKey: publicKey,
                        curve: curve
                    )
                    .mapError(SessionRepositoryError.network)
            }
            .eraseToAnyPublisher()
    }
    
    private func fetchTenantPublicKeyForCurveType(_ curveType: Curve) -> AnyPublisher<String, SessionRepositoryError> {
        switch curveType {
        case .ed25519:
            return provider
                .eddsa
                .mapError(SessionRepositoryError.tsm)
                .flatMap { tsm -> AnyPublisher<String, SessionRepositoryError> in
                    guard let publicKey = tsm.getTenantPublicKey() else {
                        return .failure(.emptyTenantPublicKey)
                    }
                    return .just(publicKey.base64URLEncodedString())
                }
                .eraseToAnyPublisher()
        case .secp256k1:
            return provider
                .ecdsa
                .mapError(SessionRepositoryError.tsm)
                .flatMap { tsm -> AnyPublisher<String, SessionRepositoryError> in
                    guard let publicKey = tsm.getTenantPublicKey() else {
                        return .failure(.emptyTenantPublicKey)
                    }
                    return .just(publicKey.base64URLEncodedString())
                }
                .eraseToAnyPublisher()
        }
    }
}
