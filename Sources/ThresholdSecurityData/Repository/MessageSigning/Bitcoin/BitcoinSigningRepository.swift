// Copyright © Blockdaemon All rights reserved.

import Combine
import CommonCryptoKit
import CryptoKit
import DIKit
import Errors
import Tsm
import ThresholdSecurityDomain
import Extensions
import ToolKit

final class BitcoinSigningRepository: MessageSigningRepositoryAPI {
    
    private let client: MessageSigningClientAPI
    private let presignature: PresignatureRepositoryAPI
    private let provider: TSMClientProviderAPI
    private let tsm: TsmECDSAClient
    private let chainPath: [Int]
    private let protocolId: String
    private let keyId: String
    private let keyType: String
    
    init(
        tsm: TsmECDSAClient,
        chainPath: [Int],
        keyId: String,
        keyType: String,
        protocolId: String,
        client: MessageSigningClientAPI = resolve(),
        provider: TSMClientProviderAPI = resolve(),
        presignature: PresignatureRepositoryAPI = resolve()
    ) {
        self.tsm = tsm
        self.chainPath = chainPath
        self.keyId = keyId
        self.keyType = keyType
        self.protocolId = protocolId
        self.client = client
        self.provider = provider
        self.presignature = presignature
    }
    
    func generatedSignedInputsFromTransactionHash(
        _ transactionHash: String,
        inputs: [SigningInput],
        userId: String
    ) -> AnyPublisher<[SignedInputs], MessageSigningRepositoryError> {
        guard let publicKey = tsm.getTenantPublicKey() else {
            return .failure(.unknown)
        }
        let presignatureIdPublisher = getPresignatureIdWithPublicKey(
            publicKey.base64URLEncodedString(),
            userId: userId
        )
        
        let partialSignaturesPublisher = presignatureIdPublisher
                .flatMap { [keyId] presignatureId -> AnyPublisher<
                    [PartialSignWithPresigResponseWithInputTxHash],
                    MessageSigningRepositoryError
                    > in
                    let signatureHashPublishers = inputs
                        .map { input in
                            Logger.shared.debug("\(input.value)")
                            return self.getPartialSignatureWithUserId(
                                userId,
                                presignatureId: presignatureId,
                                transactionInputHash: input.value.withoutHex.hexDecodedData(),
                                keyId: keyId
                            )
                            .mapError(MessageSigningRepositoryError.network)
                        }
                    
                    return Publishers.MergeMany(signatureHashPublishers)
                        .collect()
                        .eraseToAnyPublisher()
                }
            
            return partialSignaturesPublisher
                .flatMap { partialSignaturesWithTxHash in
                    self.createSignedInputsWithTransactionHash(
                        transactionHash,
                        inputs: inputs,
                        partialSignaturesWithTxHash: partialSignaturesWithTxHash
                    )
                }
                .eraseToAnyPublisher()
    }
    
    private func createSignedInputsWithTransactionHash(
        _ transactionHash: String,
        inputs: [SigningInput],
        partialSignaturesWithTxHash: [PartialSignWithPresigResponseWithInputTxHash]
    ) -> AnyPublisher<[SignedInputs], MessageSigningRepositoryError> {
        Future<[SignedInputs], MessageSigningRepositoryError> { [tsm, keyId, chainPath] promise in
            Task {
                guard let tsmChainPath = TsmChainPath(chainPath.count) else {
                    return promise(.failure(.unknown))
                }
                
                for i in 0..<chainPath.count {
                    try tsmChainPath.set(i, value: chainPath[i])
                }
                
                var transactionInputs: [SignedInputs] = []

                for (index, partialSignatureWithTxHash) in partialSignaturesWithTxHash.enumerated() {
                    let partialSignWithPresig = partialSignatureWithTxHash.partialSignWithPresig
                    let transactionInputHash = partialSignatureWithTxHash.transactionInputHash
                    let usedPresigId = partialSignWithPresig.usedPresigId
                    let partialSignatures = partialSignWithPresig.partialSignatures
                    
                    let partials = partialSignatures.map { $0.base64URLDecoded() }
                    do {
                        
                        let partialSignatureWithPublicKeyWithPresigId = try tsm.partialSign(
                            withPresig: keyId,
                            presigID: usedPresigId,
                            chainPath: tsmChainPath,
                            messageHash: transactionInputHash
                         )
                        let combinedPartialSignature = try tsm.combine(
                            partials[0],
                            partialSignatureB: partialSignatureWithPublicKeyWithPresigId.partialSignature!
                        )
                        
                        let signatureWithRecoveryId = try tsm.finalize(combinedPartialSignature)
                        
                        guard let signature = signatureWithRecoveryId.signature else {
                            return promise(.failure(.unknown))
                        }
                        let recoveryId = signatureWithRecoveryId.recoveryID
                        transactionInputs
                            .append(
                                .init(
                                    index: index,
                                    value: signature.hexEncodedString(),
                                    recoveryId: recoveryId
                                )
                            )
                    } catch {
                        return promise(.failure(.tsm(error)))
                    }
                }
                return promise(.success(transactionInputs))
            }
        }
        .eraseToAnyPublisher()
    }
    
    private func getPresignatureIdWithPublicKey(
        _ publicKey: String,
        userId: String
    ) -> AnyPublisher<String, MessageSigningRepositoryError> {
        presignature
            .generatePresignatureSessionIdForUserId(
                userId,
                publicKey: publicKey,
                keyId: keyId,
                curveType: .secp256k1
            )
            .mapError(MessageSigningRepositoryError.presignature)
            .eraseToAnyPublisher()
    }
    
    private func getPartialSignatureWithUserId(
        _ userId: String,
        presignatureId: String,
        transactionInputHash: Data,
        keyId: String
    ) -> AnyPublisher<PartialSignWithPresigResponseWithInputTxHash, NetworkError> {
        client
            .getPartialSignatureWithUserId(
                userId,
                presignatureId: presignatureId,
                message: transactionInputHash.base64URLEncodedString(),
                keyId: keyId,
                keyType: keyType,
                protocolId: protocolId
            )
            .map {
                return PartialSignWithPresigResponseWithInputTxHash(
                    partialSignWithPresig: $0,
                    transactionInputHash: transactionInputHash
                )
            }
            .eraseToAnyPublisher()
    }
}

struct PartialSignWithPresigResponseWithInputTxHash {
    let partialSignWithPresig: PartialSignWithPresigResponse
    let transactionInputHash: Data
}
