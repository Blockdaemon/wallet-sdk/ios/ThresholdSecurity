// Copyright © Blockdaemon All rights reserved.

import Foundation
import DIKit
import CommonCryptoKit
import Combine
import ThresholdSecurityDomain
import Extensions

final class MessageSigningProvider: MessageSigningProviderAPI {

    private let tsmProvider: TSMClientProviderAPI

    init(tsmProvider: TSMClientProviderAPI = resolve()) {
        self.tsmProvider = tsmProvider
    }

    public func signingRepositoryWithChainMetadata(
        _ metadata: ChainMetadata,
        keyId: String
    ) -> AnyPublisher<MessageSigningRepositoryAPI, MessageSigningProviderError> {
        switch metadata.blockchain {
        case .bitcoin:
            return tsmProvider
                .ecdsa
                .mapError(MessageSigningProviderError.tsm)
                .map { client in
                    BitcoinSigningRepository(
                        tsm: client,
                        chainPath: metadata.chainPath,
                        keyId: keyId,
                        keyType: metadata.keyType,
                        protocolId: metadata.id
                    )
                }
                .eraseToAnyPublisher()
        case .ethereum:
            return tsmProvider
                .ecdsa
                .mapError(MessageSigningProviderError.tsm)
                .map { client in
                    EthereumSigningRepository(
                        tsm: client,
                        chainId: metadata.chainId,
                        chainPath: metadata.chainPath,
                        keyId: keyId,
                        keyType: metadata.keyType,
                        protocolId: metadata.id
                    )
                }
                .eraseToAnyPublisher()
        case .solana:
            return tsmProvider
                .eddsa
                .mapError(MessageSigningProviderError.tsm)
                .map { client in
                    SolanaSigningRepository(
                        tsm: client,
                        chainId: metadata.chainId,
                        chainPath: metadata.chainPath,
                        keyId: keyId,
                        keyType: metadata.keyType,
                        protocolId: metadata.id
                    )
                }
                .eraseToAnyPublisher()
        case .polkadot:
            return tsmProvider
                .eddsa
                .mapError(MessageSigningProviderError.tsm)
                .map { client in
                    PolkadotSigningRepository(
                        tsm: client,
                        chainId: metadata.chainId,
                        chainPath: metadata.chainPath,
                        keyId: keyId,
                        keyType: metadata.keyType,
                        protocolId: metadata.id
                    )
                }
                .eraseToAnyPublisher()
        }
    }
}
