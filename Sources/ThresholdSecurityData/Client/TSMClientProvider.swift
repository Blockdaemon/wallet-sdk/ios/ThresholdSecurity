// Copyright © Blockdaemon All rights reserved.

import Combine
import DIKit
import Foundation
import Tsm
import ToolKit
import FeatureAuthenticationDomain

enum TSMClientProviderError: Error {
    case emptyUserId
    case emptyTsmTenantClient
    case emptyTsmECDSAClient
    case emptyTsmEDDSAClient
    case invalidFilePath
    case fileDoesNotExist
    case emptyTsmDatabaseForFilePath(Error)
    case tsm(Error)
}

protocol TSMClientProviderAPI {
    var ecdsa: AnyPublisher<TsmECDSAClient, TSMClientProviderError> { get }
    var eddsa: AnyPublisher<TsmEDDSAClient, TSMClientProviderError> { get }
    func nuke() -> AnyPublisher<Void, TSMClientProviderError>
}

final class TSMClientProvider: TSMClientProviderAPI {
    
    private var tsm: AnyPublisher<TsmTenantClient, TSMClientProviderError> {
        Publishers
            .Zip(
                repository
                    .configuration
                    .mapError(TSMClientProviderError.tsm),
                pathForTSMDatabase
            )
            .flatMap { (config, path) -> AnyPublisher<TsmTenantClient, TSMClientProviderError> in
                var err: NSError?

                let configuration = config.buildEmbeddedNodeConfigWithDataSourceName(
                    path,
                    password: "aVjMYyiLjj+TBPkm2xSPVKB7dRnSiDLNZHomvO0JDLShk9JsF0nO1eRvig4fk2X5CEGz62vsCXiPb4UAXx2t7rsTOFwN25AE"
                )
                let tsm = TsmNewEmbeddedTenantClient(2, 1, configuration, nil, &err)
                if let error = err {
                    Logger.shared.error("\(error)")
                    return .failure(TSMClientProviderError.tsm(error))
                }
                guard let client = tsm else {
                    return .failure(TSMClientProviderError.emptyTsmTenantClient)
                }
                return .just(client)
            }
            .eraseToAnyPublisher()
    }
    
    private var ecdsaCache: TsmECDSAClient?
    private var eddsaCache: TsmEDDSAClient?
    
    var ecdsa: AnyPublisher<TsmECDSAClient, TSMClientProviderError> {
        if let cachedValue = ecdsaCache {
            return Just(cachedValue)
                .mapError(TSMClientProviderError.tsm)
                .eraseToAnyPublisher()
        } else {
            return makeECDSA()
                .eraseToAnyPublisher()
        }
    }
    
    var eddsa: AnyPublisher<TsmEDDSAClient, TSMClientProviderError> {
        if let cachedValue = eddsaCache {
            return Just(cachedValue)
                .mapError(TSMClientProviderError.tsm)
                .eraseToAnyPublisher()
        } else {
            return makeEDDSA()
                .eraseToAnyPublisher()
        }
    }
    
    private let repository: TSMConfigurationRepositoryAPI
    private let credentials: BlockdaemonUserRepositoryAPI
    
    init(
        repository: TSMConfigurationRepositoryAPI = resolve(),
        credentials: BlockdaemonUserRepositoryAPI = resolve()
    ) {
        self.repository = repository
        self.credentials = credentials
    }
    
    func nuke() -> AnyPublisher<Void, TSMClientProviderError> {
        pathForTSMDatabase
            .flatMap { [weak self] path -> AnyPublisher<Void, TSMClientProviderError> in
                Future<Void, TSMClientProviderError> { promise in
                    do {
                        guard let fileURL = URL(string: path), let self = self else {
                            throw TSMClientProviderError.invalidFilePath
                        }
                        let filePath = fileURL.path

                        if FileManager.default.fileExists(atPath: filePath) {
                            try FileManager.default.removeItem(atPath: filePath)
                            self.ecdsaCache = nil
                            self.eddsaCache = nil
                            
                            promise(.success(()))
                        } else {
                            promise(.failure(TSMClientProviderError.fileDoesNotExist))
                        }
                    } catch {
                        promise(.failure(TSMClientProviderError.tsm(error)))
                    }
                }
                .eraseToAnyPublisher()
            }
            .eraseToAnyPublisher()
    }
    
    private func makeECDSA() -> AnyPublisher<TsmECDSAClient, TSMClientProviderError> {
        tsm.tryMap { tenantClient in
            guard let client = TsmECDSAClient(tenantClient) else {
                throw TSMClientProviderError.emptyTsmECDSAClient
            }
            return client
        }
        .handleEvents(receiveOutput: { [weak self] ecdsa in
            self?.ecdsaCache = ecdsa
        })
        .replaceError(with: TSMClientProviderError.emptyTsmECDSAClient)
        .eraseToAnyPublisher()
    }
    
    private func makeEDDSA() -> AnyPublisher<TsmEDDSAClient, TSMClientProviderError> {
        tsm.tryMap { tenantClient in
            guard let client = TsmEDDSAClient(tenantClient) else {
                throw TSMClientProviderError.emptyTsmEDDSAClient
            }
            return client
        }
        .handleEvents(receiveOutput: { [weak self] eddsa in
            self?.eddsaCache = eddsa
        })
        .replaceError(with: TSMClientProviderError.emptyTsmEDDSAClient)
        .eraseToAnyPublisher()
    }
}

extension TSMClientProvider {
    var pathForTSMDatabase: AnyPublisher<String, TSMClientProviderError> {
        credentials
            .userId
            .tryMap { userId -> String in
                guard let id = userId else { throw TSMClientProviderError.emptyUserId }
                return id
            }
            .tryMap { userId in
                try FileManager.default
                    .url(
                        for: .applicationSupportDirectory,
                        in: .userDomainMask,
                        appropriateFor: nil,
                        create: true
                    )
                    .appendingPathComponent(
                        "\(userId).tsm.sqlite",
                        isDirectory: true
                    )
                    .absoluteString
            }
            .mapError(TSMClientProviderError.tsm)
            .eraseToAnyPublisher()
    }
}
