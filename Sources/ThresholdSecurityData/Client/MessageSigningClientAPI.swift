// Copyright © Blockdaemon All rights reserved.

import Combine
import Errors

protocol MessageSigningClientAPI {
    func getPartialSignatureWithUserId(
        _ userId: String,
        presignatureId: String,
        message: String,
        keyId: String,
        keyType: String,
        protocolId: String
    ) -> AnyPublisher<PartialSignWithPresigResponse, NetworkError>
    
    func fetchSignedTransactionHex(
        _ userId: String,
        unsignedTransaction: UnsignedTransaction
    ) -> AnyPublisher<String, NetworkError>
}
