// Copyright © Blockdaemon All rights reserved.

import Combine
import CommonCryptoKit
import Errors

protocol RecoveryClientAPI {
    
    func fetchRecoveryInfoForUserId(
        _ userId: String,
        keyId: String,
        keyType: Curve,
        publicKey: String,
        tenantPublicKey: String,
        label: String,
        sessionId: String
    ) -> AnyPublisher<RecoveryInfoResponse, NetworkError>
}
