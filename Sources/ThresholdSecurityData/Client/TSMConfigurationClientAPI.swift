// Copyright © Blockdaemon All rights reserved.

import Combine
import Errors

protocol TSMConfigurationClientAPI {
    func fetchNodeConfigurationWithUserId(
        _ userId: String
    ) -> AnyPublisher<Config, NetworkError>
}
