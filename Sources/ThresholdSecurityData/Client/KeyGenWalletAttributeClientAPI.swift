// Copyright © Blockdaemon All rights reserved.

import Combine
import Errors

protocol KeyGenWalletAttributeClientAPI {
    func createWalletAttributeForKeyId(
        _ keyId: String,
        userId: String
    ) -> AnyPublisher<Void, NetworkError>
}
