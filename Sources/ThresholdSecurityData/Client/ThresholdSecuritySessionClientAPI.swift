// Copyright © Blockdaemon All rights reserved.

import Combine
import CommonCryptoKit
import Errors
import ThresholdSecurityDomain

protocol ThresholdSecuritySessionClientAPI {
    /// PublicKey for node and curve for the asset
    func generateKeygenSessionIDForUserId(
        _ userId: String,
        publicKey: String,
        curve: Curve
    ) -> AnyPublisher<String, NetworkError>
}
