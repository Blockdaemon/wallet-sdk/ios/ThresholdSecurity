// Copyright © Blockdaemon All rights reserved.

import CommonCryptoKit
import Combine
import Errors

protocol PresignatureClientAPI {
    func generatePresignatureSessionIDForUserId(
        _ userId: String,
        publicKey: String,
        count: Int,
        keyId: String,
        curve: Curve
    ) -> AnyPublisher<String, NetworkError>
}
