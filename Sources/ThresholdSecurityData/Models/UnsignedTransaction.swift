// Copyright © Blockdaemon All rights reserved.

import Foundation

struct UnsignedTransaction: Encodable {
    let unsignedTxHex: String
    let signatures: [Signature]
    let keyId: String
    let protocolId: String
}
