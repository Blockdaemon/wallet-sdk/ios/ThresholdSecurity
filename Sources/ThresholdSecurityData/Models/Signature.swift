// Copyright © Blockdaemon All rights reserved.

import Foundation

struct Signature: Encodable {
    let index: Int
    let signature: String
    let recoveryId: Int
    
    public func encode(with coder: NSCoder) {
        coder.encode(index, forKey: "index")
        coder.encode(signature, forKey: "signature")
        coder.encode(recoveryId, forKey: "recovery_id")
    }
}
