// Copyright © Blockdaemon All rights reserved.

struct Config {
    let embeddedNodeConfig: String
}

extension Config {
    
    // TODO: Rewrite this when we can initialize TSM with a model rather than a string/TOML value.
    func buildEmbeddedNodeConfigWithDataSourceName(
        _ name: String,
        password: String
    ) -> String {
        let range = embeddedNodeConfig.range(of: "[MPC]")!
        let configPart = String(embeddedNodeConfig[range.lowerBound...])
        // Ensure quotes are not escaped
            .replacingOccurrences(of: "\\\"", with: "\"")
        
        var finalConfigPart = ""
        if !configPart.hasPrefix("[MPC]") {
            // Ensure MPC is the start of this section
            finalConfigPart = "[MPC]\n\(configPart)"
        } else {
            finalConfigPart = configPart
        }
        
        return """
            [Mode]
            Embedded = true
            
            \(finalConfigPart)
            [Player]
            Index = 0
            ExportWhiteList = [\"*\"]
            
            [Database]
            DriverName = "sqlite3"
            DataSourceName = "\(name)"
            EncryptorMasterPassword = "\(password)"
            MaxIdleConns = 1
            MaxOpenConns = 1
            
            [SEPD19S]
            EnableERSExport = true
            EnableExport = true
            EnableShareBackup = true
            
            [DKLS19]
            EnableBIP32ExportSeed = true
            EnableERSExport = true
            EnableExport = true
            EnableShareBackup = true
            """
    }
}
