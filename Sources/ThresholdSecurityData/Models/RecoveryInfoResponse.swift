// Copyright © Blockdaemon All rights reserved.

import Extensions
import Foundation

struct RecoveryInfoResponse: Decodable {
    let partialRecoveryInfos: [String]
}

extension RecoveryInfoResponse {
    var recoveryData: [Data] {
        partialRecoveryInfos
            .map { "\($0)" }
            .map { $0.base64URLDecoded() }
    }
}
