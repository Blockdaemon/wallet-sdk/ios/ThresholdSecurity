// Copyright © Blockdaemon All rights reserved.

import Foundation

struct PartialSignWithPresigResponse: Decodable {
    let partialSignatures: [String]
    let usedPresigId: String
}
