// Copyright © Blockdaemon All rights reserved.

import CloudKit
import Combine
import DIKit
import Extensions
import ToolKit

public enum CloudKitRepositoryError: Error {
    case cloudKitUnavailable
    case unknown(Error)
}

public protocol CloudKitRepositoryAPI {
    
    /// Checks to see if CloudKit is available. If it's not the user
    /// isn't signed into their Apple ID or has iCloud disabled.
    /// This will return successfully if running on your iOS simulator for testing purposes.
    func checkIfCloudKitIsAvailable() -> AnyPublisher<Void, CloudKitRepositoryError>
    
    /// Saves the encryption key to CloudKit
    func saveEncryptionKey(
        _ encryptionKey: Data,
        userId: String
    ) -> AnyPublisher<Void, Error>
    
    /// Fetches the encryptionKey for the given userId.
    func fetchEncryptionKeyForUserId(
        _ userId: String
    ) -> AnyPublisher<Data?, Never>
    
    /// Sets the user's encryptionKey value to `nil`
    func clearEncryptionKeyForUserId(
        _ userId: String
    ) -> AnyPublisher<Void, Error>
}

final class CloudKitRepository: CloudKitRepositoryAPI {
    
    private enum CloudKit {
        static let recordType = "encryption_key_record"
        static let encryptedKey = "bd_encryption_key_data"
        static let userId = "bd_encryption_key_user_id"
    }
    
    private var containerId: AnyPublisher<String, Never> {
        Deferred {
            Future<String, Never> { promise in
                if let path = Bundle.main.path(forResource: "Info", ofType: "plist"),
                   let dict = NSDictionary(contentsOfFile: path) as? [String: AnyObject],
                   let containerID = dict["CloudKitContainerID"] as? String {
                    promise(.success(containerID))
                } else {
                    fatalError("CloudKitContainerID not found in Info.plist")
                }
            }
        }
        .eraseToAnyPublisher()
    }
    
    func checkIfCloudKitIsAvailable() -> AnyPublisher<Void, CloudKitRepositoryError> {
#if targetEnvironment(simulator)
        return Just(())
            .setFailureType(to: CloudKitRepositoryError.self)
            .eraseToAnyPublisher()
#else
        Future<Void, CloudKitRepositoryError> { promise in
                Task {
                    do {
                        let status = try await CKContainer
                            .default()
                            .accountStatus()
                        switch status {
                        case .available:
                            return promise(.success(()))
                        case .couldNotDetermine,
                                .noAccount,
                                .restricted,
                                .temporarilyUnavailable:
                            Logger.shared.error("CloudKit is unavailable: \(status)")
                            return promise(.failure(.cloudKitUnavailable))
                        @unknown default:
                            impossible()
                        }
                    } catch {
                        return promise(.failure(.unknown(error)))
                    }
                }
            }
            .eraseToAnyPublisher()
#endif
    }
    
    func fetchEncryptionKeyForUserId(
        _ userId: String
    ) -> AnyPublisher<Data?, Never> {
        containerId
            .flatMap { containerId in
                Future<Data?, Never> { promise in
                    let predicate = NSPredicate(format: "%K == %@", CloudKit.userId, userId)
                    let query = CKQuery(recordType: CloudKit.recordType, predicate: predicate)

                    CKContainer(identifier: containerId)
                        .privateCloudDatabase
                        .fetch(
                            withQuery: query,
                            desiredKeys: [CloudKit.encryptedKey],
                            resultsLimit: 1
                        ) { result in
                        DispatchQueue.main.async {
                            switch result {
                            case .failure(let error):
                                Logger.shared.error("Fetching encryption key for user \(userId) failed: \(error)")
                                promise(.success(nil))
                            case .success((let matchResults, _)):
                                guard let record = matchResults.first?.1 else {
                                    Logger.shared.debug("No records matched for user \(userId).")
                                    promise(.success(nil))
                                    return
                                }
                                switch record {
                                case .success(let fetchedRecord):
                                    if let encryptionKey = fetchedRecord[CloudKit.encryptedKey] as? Data {
                                        Logger.shared.debug("Fetched encryption key for user \(userId).")
                                        promise(.success(encryptionKey))
                                    } else {
                                        Logger.shared.debug("Encryption key for user \(userId) not found or not set.")
                                        promise(.success(nil))
                                    }
                                case .failure(let error):
                                    Logger.shared.error("Fetching encryption key for user \(userId) failed: \(error)")
                                    promise(.success(nil))
                                }
                            }
                        }
                    }
                }
            }
            .eraseToAnyPublisher()
    }
    
    func saveEncryptionKey(
        _ encryptionKey: Data,
        userId: String
    ) -> AnyPublisher<Void, Error> {
#if targetEnvironment(simulator)
        return Just(())
            .setFailureType(to: Error.self)
            .eraseToAnyPublisher()
#else
        containerId
            .flatMap { containerId in
                Future<Void, Error> { promise in
                    let record = CKRecord(
                        recordType: CloudKit.recordType,
                        recordID: .init(recordName: UUID().uuidString)
                    )
                    record[CloudKit.encryptedKey] = encryptionKey as CKRecordValue
                    record[CloudKit.userId] = userId as CKRecordValue
                    
                    CKContainer(identifier: containerId)
                        .privateCloudDatabase
                        .save(record) { (_, error) in
                            DispatchQueue.main.async {
                                if let error = error {
                                    Logger.shared.error("Saving to CloudKit Failed: \(error)")
                                    promise(.failure(error))
                                } else {
                                    Logger.shared.debug("Saved encryption key to CloudKit for user \(userId).")
                                    promise(.success(()))
                                }
                            }
                        }
                }
            }
            .eraseToAnyPublisher()
        #endif
    }
    
    func clearEncryptionKeyForUserId(
        _ userId: String
    ) -> AnyPublisher<Void, Error> {
        containerId
            .flatMap { containerId in
                Future<Void, Error> { promise in
                    let predicate = NSPredicate(format: "%K == %@", CloudKit.userId, userId)
                    let query = CKQuery(recordType: CloudKit.recordType, predicate: predicate)
                    
                    CKContainer(identifier: containerId)
                        .privateCloudDatabase
                        .fetch(
                            withQuery: query,
                            resultsLimit: 1
                        ) { result in
                            DispatchQueue.main.async {
                                switch result {
                                case .failure(let error):
                                    Logger.shared.error("Error fetching records: \(error)")
                                    promise(.failure(error))
                                case .success((let matchResults, _)):
                                    guard let recordID = matchResults.first?.0 else {
                                        Logger.shared.debug("No record found for user \(userId) to clear encryption key.")
                                        promise(.success(()))
                                        return
                                    }

                                    CKContainer(identifier: containerId)
                                        .privateCloudDatabase
                                        .delete(withRecordID: recordID) { (recordID, error) in
                                            if let error = error {
                                                Logger.shared.error("Error deleting record: \(error)")
                                                promise(.failure(error))
                                            } else {
                                                Logger.shared.debug("Encryption key successfully cleared for user \(userId).")
                                                promise(.success(()))
                                            }
                                        }
                                }
                            }
                        }
                }
            }
            .eraseToAnyPublisher()
    }
}
