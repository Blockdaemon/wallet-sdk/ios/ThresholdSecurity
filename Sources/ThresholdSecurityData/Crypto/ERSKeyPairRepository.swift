// Copyright © Blockdaemon All rights reserved.

import CryptoKit
import Combine
import CommonCryptoKit
import Foundation
import ThresholdSecurityDomain

final class ERSKeyPairRepository: ERSKeyPairRepositoryAPI {
    
    struct Key {
        let privateKey: Data
        let publicKey: Data
    }
    
    var keyPair: AnyPublisher<ERSKeyPair, ERSKeyPairRepositoryError> {
        generateRSAKeyPair()
            .flatMap { key in
                self.password
                    .flatMap { password in
                        self.encryptPrivateKey(
                            privateKey: key.privateKey,
                            password: password
                        )
                        .map {
                            ERSKeyPair(
                                publicKey: key.publicKey,
                                encryptedPrivateKey: $0,
                                password: password
                            )
                        }
                        .eraseToAnyPublisher()
                    }
            }
            .eraseToAnyPublisher()
    }
    
    func decryptPrivateKeyWithPassword(
        _ password: String,
        privateKey: Data
    ) -> AnyPublisher<Data, ERSKeyPairRepositoryError> {
        do {
            let key = HKDF<SHA256>.deriveKey(inputKeyMaterial: SymmetricKey(data: Data(password.utf8)), outputByteCount: 32)
            guard let sealedBox = try? AES.GCM.SealedBox(combined: privateKey) else {
                return .failure(.privateKeyDecrypionFailure)
            }
            
            let decryptedData = try AES.GCM.open(sealedBox, using: key)
            return .just(decryptedData)
        } catch {
            return .failure(.privateKeyDecrypionFailure)
        }
    }
    
    private var password: AnyPublisher<String, ERSKeyPairRepositoryError> {
        var randomBytes = [UInt8](repeating: 0, count: 16)
        let result = SecRandomCopyBytes(kSecRandomDefault, randomBytes.count, &randomBytes)
        guard result == errSecSuccess else {
            return .failure(.passwordGeneration)
        }
        let password = randomBytes.map { String(format: "%02hhx", $0) }.joined()
        return .just(password)
    }

    private func generateRSAKeyPair() -> AnyPublisher<Key, ERSKeyPairRepositoryError> {
        let attributes: [String: Any] = [
            kSecAttrKeyType as String: kSecAttrKeyTypeRSA,
            kSecAttrKeySizeInBits as String: 2048,
        ]
        
        var error: Unmanaged<CFError>?
        guard let privateKey = SecKeyCreateRandomKey(attributes as CFDictionary, &error) else {
            return .failure(.privateKeyGeneration)
        }
        guard let publicKey = SecKeyCopyPublicKey(privateKey) else {
            return .failure(.publicKeyGeneration)
        }
        
        return Publishers.Zip(
            secKeyToData(privateKey),
            secKeyToData(publicKey)
        )
        .map { Key(privateKey: $0.0, publicKey: $0.1) }
        .eraseToAnyPublisher()
    }

    private func secKeyToData(_ secKey: SecKey) -> AnyPublisher<Data, ERSKeyPairRepositoryError> {
        var error: Unmanaged<CFError>?
        guard let data = SecKeyCopyExternalRepresentation(secKey, &error) as Data? else {
            return .failure(.secKeyToDataFailure)
        }
        return .just(data)
    }

    private func encryptPrivateKey(privateKey: Data, password: String) -> AnyPublisher<Data, ERSKeyPairRepositoryError> {
        do {
            let key = HKDF<SHA256>.deriveKey(inputKeyMaterial: SymmetricKey(data: Data(password.utf8)), outputByteCount: 32)
            let sealedBox = try AES.GCM.seal(privateKey, using: key)
            guard let combined = sealedBox.combined else {
                return .failure(.privateKeyEncryptionFailure)
            }
            return .just(combined)
        } catch {
            return .failure(.privateKeyEncryptionFailure)
        }
    }
}
