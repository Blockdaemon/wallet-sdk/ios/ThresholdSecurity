// Copyright © Blockdaemon All rights reserved.

import Combine
import CommonCryptoKit
import CryptoKit
import DIKit
import Extensions
import Errors
import ToolKit
import Tsm
import ThresholdSecurityDomain

final class EDDSARecoveryRepository: RecoveryRepositoryAPI {
    
    private let client: RecoveryClientAPI
    private let provider: TSMClientProviderAPI
    private let keyIdDirectory: KeyIdDirectoryAPI

    init(
        client: RecoveryClientAPI = resolve(),
        provider: TSMClientProviderAPI = resolve(),
        keyIdDirectory: KeyIdDirectoryAPI = resolve()
    ) {
        self.client = client
        self.provider = provider
        self.keyIdDirectory = keyIdDirectory
    }
    
    func fetchRecoveryInfoForUserId(
        _ userId: String,
        sessionId: String,
        ersLabel: String,
        ersPublicKey: Data
    ) -> AnyPublisher<Data, RecoveryRepositoryError> {
        guard let keyId = keyIdDirectory[userId, .ed25519]?.identifier else {
            return .failure(.emptyKeyId)
        }
        let tenantPublicKey = provider
            .eddsa
            .mapError(RecoveryRepositoryError.tsm)
            .flatMap { tsm -> AnyPublisher<Data, RecoveryRepositoryError> in
                guard let publicKey = tsm.getTenantPublicKey() else {
                    return .failure(.nilPublicKey)
                }
                return .just(publicKey)
            }
            .eraseToAnyPublisher()
        
        return tenantPublicKey
            .flatMap { publicKey in
                Publishers
                    .Zip(
                        self.fetchRemoteRecoveryData(
                            publicKey,
                            userId: userId,
                            sessionId: sessionId,
                            keyId: keyId,
                            ersLabel: ersLabel,
                            ersPublicKey: ersPublicKey
                        ),
                        self.fetchLocalRecoveryDataFromTSM(
                            sessionId: sessionId,
                            keyId: keyId,
                            ersPublicKey: ersPublicKey,
                            label: ersLabel
                        )
                    )
                    .map {
                        RecoveryData(
                            remoteRecoveryData: $0.0,
                            localRecoveryData: $0.1
                        )
                    }
            }
            .flatMap {
                self.fetchRecoveryDataFromTSMWithPartialRecoveryInfo(
                    $0.remoteRecoveryData,
                    localRecoveryInfo: $0.localRecoveryData,
                    ersPublicKey: ersPublicKey,
                    label: ersLabel
                )
            }
            .eraseToAnyPublisher()
    }
    
    func fetchRecoveryKeyWithRecoveryInfo(
        _ recoveryInfo: Data,
        ersPrivateKey: Data,
        ersLabel: String
    ) -> AnyPublisher<RecoveryKey, RecoveryRepositoryError> {
        Future { promise in
            var err: NSError?
            let key = TsmRecoverKeyEdDSA(
                recoveryInfo,
                ersPrivateKey,
                ersLabel.data(using: .utf8),
                &err
            )
            if let error = err {
                Logger.shared.error("Error fetching recovery key: \(error)")
                promise(.failure(.unknown))
            } else if let key = key, let privateKey = key.privateKey, let code = key.masterChainCode {
                promise(.success(.init(curve: .ed25519, privateKey: privateKey, chainCode: code)))
            } else {
                Logger.shared.error("Empty Recovery Key")
                promise(.failure(.unknown))
            }
        }
        .eraseToAnyPublisher()
    }
    
    private func fetchRemoteRecoveryData(
        _ tenantPublicKey: Data,
        userId: String,
        sessionId: String,
        keyId: String,
        ersLabel: String,
        ersPublicKey: Data
    ) -> AnyPublisher<[Data], RecoveryRepositoryError> {
        client
            .fetchRecoveryInfoForUserId(
                userId,
                keyId: keyId,
                keyType: Curve.ed25519,
                publicKey: ersPublicKey.base64URLEncodedString(),
                tenantPublicKey: tenantPublicKey.base64URLEncodedString(),
                label: ersLabel,
                sessionId: sessionId
            )
            .mapError(RecoveryRepositoryError.network)
            .map(\.recoveryData)
            .eraseToAnyPublisher()
    }
    
    private func fetchLocalRecoveryDataFromTSM(
        sessionId: String,
        keyId: String,
        ersPublicKey: Data,
        label: String
    ) -> AnyPublisher<Data, RecoveryRepositoryError> {
        provider
            .eddsa
            .mapError(RecoveryRepositoryError.tsm)
            .tryMap { tsm -> Data in
                do {
                    let localRecoveryInfo = try tsm.partialRecoveryInfo(
                        sessionId,
                        keyID: keyId,
                        ersPublicKey: ersPublicKey.encodePublicKeyToDER()!,
                        label: label.data(using: .utf8)
                    )
                    return localRecoveryInfo
                } catch {
                    throw error
                }
            }
            .mapError(RecoveryRepositoryError.combinationError)
            .eraseToAnyPublisher()
    }
    
    private func fetchRecoveryDataFromTSMWithPartialRecoveryInfo(
        _ remoteRecoveryInfo: [Data],
        localRecoveryInfo: Data,
        ersPublicKey: Data,
        label: String
    ) -> AnyPublisher<Data, RecoveryRepositoryError> {
        provider
            .eddsa
            .mapError(RecoveryRepositoryError.tsm)
            .flatMap { tsm -> AnyPublisher<Data, RecoveryRepositoryError> in
                var err: NSError?
                var combined: Data? = Data()
                
                for info in [localRecoveryInfo] + remoteRecoveryInfo {
                    if let existingCombined = combined {
                        combined = TsmCombinePartialRecoveryInfos(existingCombined, info, &err)
                        if let error = err, combined == nil {
                            return .failure(.combinationError(error))
                        }
                    }
                }
                guard let ersLabel = label.data(using: .utf8) else {
                    return .failure(.nilDataError)
                }
                err = nil
                guard let combined = combined else {
                    return .failure(.nilPublicKey)
                }
                let result = TsmFinalizeRecoveryInfo(combined, ersPublicKey.encodePublicKeyToDER()!, ersLabel, &err)
                if let error = err {
                    Logger.shared.error("EDDSA ERS Recovery Error: \(error)")
                    return .failure(.finalizationError(error))
                } else if let result = result {
                    return .just(result)
                } else {
                    return .failure(.unknown)
                }
            }
        .eraseToAnyPublisher()
    }
}
