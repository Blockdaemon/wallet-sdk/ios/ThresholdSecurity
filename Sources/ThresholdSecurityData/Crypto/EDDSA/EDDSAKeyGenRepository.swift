// Copyright © Blockdaemon All rights reserved.

import CryptoKit
import CommonCryptoKit
import Combine
import DIKit
import Extensions
import Foundation
import ThresholdSecurityDomain
import Tsm
import ToolKit

final class EDDSAKeyGenRepository: KeyGenRepositoryAPI {
    
    private let tsmClientProvider: TSMClientProviderAPI
    private let walletAttributeRepository: KeyGenWalletAttributeRepositoryAPI
    
    init(tsmClientProvider: TSMClientProviderAPI = resolve(),
         walletAttributeRepository: KeyGenWalletAttributeRepositoryAPI = resolve()) {
        self.tsmClientProvider = tsmClientProvider
        self.walletAttributeRepository = walletAttributeRepository
    }
    
    func generateKeysForSessionId(
        _ sessionId: String,
        userId: String
    ) -> AnyPublisher<KeyGen, KeyGenRepositoryError> {
        tsmClientProvider
            .eddsa
            .mapError(KeyGenRepositoryError.tsmClientError)
            .flatMap { eddsa in
                self.generateKeyIdForSessionId(sessionId, tsm: eddsa)
                    .flatMap { keyId in
                        self.generateLocalKeyShareForKeyId(keyId, tsm: eddsa)
                            .map { data in
                                    .init(keyId: .init(curve: .ed25519, identifier: keyId), keyshare: data)
                            }
                            .eraseToAnyPublisher()
                    }
                    .flatMap { keyGen in
                        self.walletAttributeRepository.createWalletAttributeForKeyId(keyGen.identifier, userId: userId)
                            .mapError(KeyGenRepositoryError.network)
                            .map {
                                keyGen
                            }
                    }
                    .eraseToAnyPublisher()
            }
            .eraseToAnyPublisher()
    }
    
    func restoreKeysForSessionId(
        _ sessionId: String,
        backup: Data
    ) -> AnyPublisher<KeyGen, KeyGenRepositoryError> {
        tsmClientProvider
            .eddsa
            .mapError(KeyGenRepositoryError.tsmClientError)
            .flatMap { eddsa in
                self.generateKeyIdFromBackup(backup, tsm: eddsa)
                    .map { keyId in
                        KeyGen.init(keyId: .init(curve: .ed25519, identifier: keyId), keyshare: backup)
                    }
                    .eraseToAnyPublisher()
            }
            .eraseToAnyPublisher()
    }
    
    func generateBackupKeyShareForKeyId(
        _ keyId: String
    ) -> AnyPublisher<KeyGen, KeyGenRepositoryError> {
        tsmClientProvider
            .eddsa
            .mapError(KeyGenRepositoryError.tsmClientError)
            .flatMap { eddsa in
                self.generateLocalKeyShareForKeyId(keyId, tsm: eddsa)
                    .map { keyShare in
                        KeyGen(keyId: .init(curve: .ed25519, identifier: keyId), keyshare: keyShare)
                    }
                    .eraseToAnyPublisher()
            }
            .eraseToAnyPublisher()
    }
    
    private func generateKeyIdFromBackup(
        _ backup: Data,
        tsm: TsmEDDSAClient
    ) -> AnyPublisher<String, KeyGenRepositoryError> {
        Future { [tsm] promise in
            var err: NSError?
            let keyId = tsm.restoreShare(backup, error: &err)
            if let error = err {
                Logger.shared.error("Error generating key id: \(String(describing: err))")
                promise(.failure(.emptyKeyId(error)))
                return
            }
            
            promise(.success(keyId))
        }
        .eraseToAnyPublisher()
    }
    
    private func generateKeyIdForSessionId(
        _ sessionId: String,
        tsm: TsmEDDSAClient
    ) -> AnyPublisher<String, KeyGenRepositoryError> {
        Future { promise in

            Logger.shared.debug("EDDSA Session ID: \(sessionId)")
            var err: NSError?
            let keyId = tsm.keygen(
                withSessionID: sessionId,
                curveName: Curve.ed25519.rawValue,
                error: &err
            )
            if let error = err {
                Logger.shared.error("Error generating key id: \(String(describing: err))")
                promise(.failure(.emptyKeyId(error)))
                return
            }

            Logger.shared.debug("EDDSA Key ID created: \(keyId)")
            promise(.success(keyId))
        }
        .eraseToAnyPublisher()
    }
    
    private func generateLocalKeyShareForKeyId(
        _ keyId: String,
        tsm: TsmEDDSAClient
    ) -> AnyPublisher<Data, KeyGenRepositoryError> {
        Future { promise in
            do {
                let localKeyShare = try tsm.backupShare(keyId)
                promise(.success(localKeyShare))
            } catch {
                Logger.shared.error("Error generating local key share: \(error)")
                promise(.failure(.tsmClientError(error)))
            }
        }
        .eraseToAnyPublisher()
    }
}
