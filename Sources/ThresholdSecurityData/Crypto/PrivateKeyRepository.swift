// Copyright © Blockdaemon All rights reserved.

import Combine
import CryptoKit
import DIKit
import Extensions
import ThresholdSecurityDomain
import Foundation
import Security
import ToolKit

extension PrivateKeyRepositoryError {
    init(from status: OSStatus) {
        switch status {
        case errSecItemNotFound:
            self = .itemNotFound
        case errSecDuplicateItem:
            self = .duplicateItem
        case errSecInteractionNotAllowed:
            self = .interactionNotAllowed
        case errSecDecode:
            self = .decodeError
        case errSecAuthFailed:
            self = .authenticationFailed
        default:
            self = .keychainError(status.secErrorMessage)
        }
    }
}

final class PrivateKeyRepository: PrivateKeyRepositoryAPI {

    var temporaryPassword: AnyPublisher<Data, PrivateKeyRepositoryError> {
        Future<Data, PrivateKeyRepositoryError> { promise in
            let length = 16
            var bytes = [UInt8](repeating: 0, count: length)
            let status = SecRandomCopyBytes(kSecRandomDefault, length, &bytes)
            guard status == errSecSuccess else {
                promise(.failure(.temporaryPasswordCreationFailed))
                return
            }
            promise(.success(Data(bytes: bytes, count: length)))
        }
        .eraseToAnyPublisher()
    }

    private static let name: String = "WalletSDKPrivateKey"

    enum Storage: String, CaseIterable {
        case generic, key

        var secClass: CFString {
            switch self {
            case .generic:
                return kSecClassGenericPassword
            case .key:
                return kSecClassKey
            }
        }
    }
    
    private let cloud: CloudKitRepositoryAPI
    
    init(cloud: CloudKitRepositoryAPI = resolve()) {
        self.cloud = cloud
    }

    func deleteEncryptionKeyForUserId(
        _ userId: String
    ) -> AnyPublisher<Void, PrivateKeyRepositoryError> {
        #if DEBUG
        return Publishers
            .Zip(
                cloud
                    .clearEncryptionKeyForUserId(userId)
                    .mapError(PrivateKeyRepositoryError.cloudKit),
                deleteEncryptionKeyFromKeychainForUserId(userId)
            )
            .mapToVoid()
            .eraseToAnyPublisher()
        #else
        return .just(())
        #endif
    }

    
    func createOrFetchEncryptionKeyForUserId(_ userId: String) -> AnyPublisher<SymmetricKey, PrivateKeyRepositoryError> {
        createOrFetchEncryptionKeyDataForUserId(userId)
            .tryMap { data in
                try SymmetricKey(rawRepresentation: data)
            }
            .mapError(PrivateKeyRepositoryError.invalidSymmetricKey)
            .eraseToAnyPublisher()
    }
    
    func createOrFetchEncryptionKeyDataForUserId(
        _ userId: String
    ) -> AnyPublisher<Data, PrivateKeyRepositoryError> {
        cloud
            .checkIfCloudKitIsAvailable()
            .mapError(PrivateKeyRepositoryError.cloudKit)
            .flatMap { _ in
                self.fetchEncryptionKeyDataForUserId(userId)
                    .catch { error -> AnyPublisher<Data, PrivateKeyRepositoryError> in
                        switch error {
                        case .itemNotFound:
                            return self.cloud.fetchEncryptionKeyForUserId(userId)
                                .flatMap { keyData -> AnyPublisher<Data, PrivateKeyRepositoryError> in
                                    if let keyData = keyData {
                                        // If a key exists in CloudKit, save it to the keychain.
                                        // The only way this happens is if the keychain is somehow wiped.
                                        return self.saveEncryptionKeyFromCloudKitToKeychain(
                                            keyData,
                                            userId: userId
                                        )
                                    } else {
                                        // If no key exists, create a new one, save it to CloudKit and the keychain.
                                        return self.createEncryptionKeyDataForUserId(userId)
                                            .flatMap { newData in
                                                self.cloud.saveEncryptionKey(newData, userId: userId)
                                                    .map { _ in newData }
                                                    .mapError(PrivateKeyRepositoryError.cloudKit)
                                                    .eraseToAnyPublisher()
                                            }
                                            .eraseToAnyPublisher()
                                    }
                                }
                                .eraseToAnyPublisher()
                        default:
                            return Fail(error: error).eraseToAnyPublisher()
                        }
                    }
                    .eraseToAnyPublisher()
            }
            .eraseToAnyPublisher()
    }
    
    func fetchRemoteEncryptionKeyDataForUserId(
        _ userId: String
    ) -> AnyPublisher<Data?, PrivateKeyRepositoryError> {
        cloud
            .fetchEncryptionKeyForUserId(userId)
            .mapError(PrivateKeyRepositoryError.cloudKit)
            .eraseToAnyPublisher()
    }
    
    func fetchEncryptionKeyDataForUserId(
        _ userId: String
    ) -> AnyPublisher<Data, PrivateKeyRepositoryError> {
        Future { promise in
            DispatchQueue.global(qos: .userInitiated).async {
                let query: [CFString: Any] = [
                    kSecClass: kSecClassGenericPassword,
                    kSecAttrAccount: userId,
                    kSecAttrLabel: PrivateKeyRepository.name,
                    kSecReturnData: true,
                    kSecAttrSynchronizable: kSecAttrSynchronizableAny
                ]

                var itemRef: CFTypeRef?
                let status = SecItemCopyMatching(query as CFDictionary, &itemRef)
                DispatchQueue.main.async {
                    guard status == errSecSuccess else {
                        promise(.failure(PrivateKeyRepositoryError(from: status)))
                        return
                    }
                    
                    promise(.success(itemRef as! Data))
                }
            }
        }
        .eraseToAnyPublisher()
    }
    
    // MARK: - Private
    
    private func createEncryptionKeyDataForUserId(
        _ userId: String
    ) -> AnyPublisher<Data, PrivateKeyRepositoryError> {
        Future<Data, PrivateKeyRepositoryError> { promise in
            DispatchQueue.global(qos: .userInitiated).async {
                let key = SymmetricKey(size: .bits256)
                
                let query: [CFString: Any] = [
                    kSecClass: kSecClassGenericPassword,
                    kSecAttrAccount: userId,
                    kSecAttrLabel: PrivateKeyRepository.name,
                    kSecAttrSynchronizable: kSecAttrSynchronizableAny,
                    kSecValueData: key.rawRepresentation
                ]
                
                let status = SecItemAdd(query as CFDictionary, nil)
                DispatchQueue.main.async {
                    if status == errSecSuccess {
                        promise(.success(key.rawRepresentation))
                    } else {
                        promise(.failure(PrivateKeyRepositoryError(from: status)))
                    }
                }
            }
        }
        .eraseToAnyPublisher()
    }
    
    private func deleteEncryptionKeyFromKeychainForUserId(
        _ userId: String
    ) -> AnyPublisher<Void, PrivateKeyRepositoryError> {
        Future { promise in
            let query: [CFString: Any] = [
                kSecClass: kSecClassGenericPassword,
                kSecAttrAccount: userId,
                kSecAttrSynchronizable: kSecAttrSynchronizableAny
            ]

            let status = SecItemDelete(query as CFDictionary)

            DispatchQueue.main.async {
                if status == errSecSuccess || status == errSecItemNotFound {
                    promise(.success(()))
                } else {
                    let error = PrivateKeyRepositoryError(from: status)
                    #if DEBUG
                    Logger.shared.debug("Failed to delete: \(error.localizedDescription)")
                    #endif
                    promise(.failure(error))
                }
            }
        }
        .eraseToAnyPublisher()
    }
    
    // MARK: - Cloud
    
    private func saveEncryptionKeyFromCloudKitToKeychain(
        _ data: Data,
        userId: String
    ) -> AnyPublisher<Data, PrivateKeyRepositoryError> {
        Future<Data, PrivateKeyRepositoryError> { promise in
            DispatchQueue.global(qos: .userInitiated).async {
                let query: [CFString: Any] = [
                    kSecClass: kSecClassGenericPassword,
                    kSecAttrAccount: userId,
                    kSecAttrLabel: PrivateKeyRepository.name,
                    kSecAttrSynchronizable: kSecAttrSynchronizableAny,
                    kSecValueData: data
                ]
                
                let addStatus = SecItemAdd(query as CFDictionary, nil)
                DispatchQueue.main.async {
                    if addStatus == errSecSuccess {
                        promise(.success(data))
                    } else {
                        promise(.failure(PrivateKeyRepositoryError(from: addStatus)))
                    }
                }
            }
        }
        .eraseToAnyPublisher()
    }

}
