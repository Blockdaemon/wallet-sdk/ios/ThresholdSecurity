// Copyright © Blockdaemon All rights reserved.

import Combine
import DIKit
import ThresholdSecurityDomain

final class TSMGlobalRepository: TSMGlobalRepositoryAPI {
    
    private let tsmClientProvider: TSMClientProviderAPI
    
    init(tsmClientProvider: TSMClientProviderAPI = resolve()) {
        self.tsmClientProvider = tsmClientProvider
    }
    
    func nuke() -> AnyPublisher<Void, TSMGlobalRepositoryError> {
        tsmClientProvider
            .nuke()
            .replaceError(with: TSMGlobalRepositoryError.emptyTsmDatabaseForFilePath)
            .eraseToAnyPublisher()
    }
}
