// Copyright © Blockdaemon All rights reserved.

import Combine
import CommonCryptoKit
import DIKit
import Errors
import Extensions
import Foundation
import NetworkKit
import ToolKit
import ThresholdSecurityDomain

typealias FeatureThresholdSecurityClientAPI = ThresholdSecuritySessionClientAPI &
                                              PresignatureClientAPI &
                                              MessageSigningClientAPI &
                                              RecoveryClientAPI &
                                              TSMConfigurationClientAPI &
                                              KeyGenWalletAttributeClientAPI

final class APIClient: FeatureThresholdSecurityClientAPI {
    
    private enum Path: String {
        case startkeygen = "start-keygen"
        case startpresiggen = "start-presig-gen"
        case partialSignatures = "partial-signatures"
        case registertenantpublickey = "register-tenant-public-key"
        case generatesignedtxhex = "generate-signed-tx-hex"
        case recoveryinfo = "partial-recovery-infos"
        case nodeconfig = "node-config"
        case attribute = "wallet-attributes"
        
        static func makePathWithUserId(
            _ userId: String,
            path: Path
        ) -> [String] {
            ["waas", "v1", "application-users", userId, "wallet-operations", path.rawValue]
        }
    }

    private enum Parameter {
        static let tenantpublickey = "tenant-public-key"
        static let keyType = "key-type"
        static let erspublickey = "ers-public-key"
        static let curvename = "curve-name"
        static let count = "presig-count"
        static let keyid = "key-id"
        static let erslabel = "ers-label"
        static let sessionid = "session-id"
        static let chainpath = "chain-path"
        static let message = "message"
        static let protocolId = "protocol-id"
        static let presigid = "presig-id"
    }
    
    private let networkAdapter: NetworkAdapterAPI
    private let requestBuilder: RequestBuilder
    
    // MARK: - Init

    init(
        requestBuilder: RequestBuilder = resolve(),
        networkAdapter: NetworkAdapterAPI = resolve()
    ) {
        self.requestBuilder = requestBuilder
        self.networkAdapter = networkAdapter
    }
    
    // MARK: - TSMConfigurationClientAPI
    
    func fetchNodeConfigurationWithUserId(
        _ userId: String
    ) -> AnyPublisher<Config, NetworkError> {
        
        struct Response: Decodable {
            let config: String
        }
        
        let request = requestBuilder.get(
            path: Path.makePathWithUserId(userId, path: .nodeconfig),
            authenticated: true
        )!
        
        return networkAdapter
            .perform(request: request, responseType: Response.self)
            .map(\.config)
            .map { response in
                Config(embeddedNodeConfig: response)
            }
            .eraseToAnyPublisher()
    }
    
    // MARK: - ThresholdSecuritySessionClientAPI
    
    func generateKeygenSessionIDForUserId(
        _ userId: String,
        publicKey: String,
        curve: Curve
    ) -> AnyPublisher<String, NetworkError> {
        struct Response: Decodable {
            let sessionId: String
        }
        
        let request = requestBuilder.get(
            path: Path.makePathWithUserId(userId, path: .startkeygen),
            parameters: [
                .init(name: Parameter.keyType, value: curve == .ed25519 ? "eddsa" : "ecdsa"),
                .init(name: Parameter.tenantpublickey, value: publicKey),
                .init(name: Parameter.curvename, value: curve.rawValue)
            ],
            authenticated: true
        )!

        return networkAdapter
            .perform(request: request, responseType: Response.self)
            .map(\.sessionId)
            .eraseToAnyPublisher()
    }
    
    // MARK: - PresignatureClientAPI
    
    func generatePresignatureSessionIDForUserId(
        _ userId: String,
        publicKey: String,
        count: Int,
        keyId: String,
        curve: Curve
    ) -> AnyPublisher<String, NetworkError> {
        struct Response: Decodable {
            let sessionId: String
        }
        
        let request = requestBuilder.get(
            path: Path.makePathWithUserId(userId, path: .startpresiggen),
            parameters: [
                .init(name: Parameter.keyType, value: curve == .ed25519 ? "eddsa" : "ecdsa"),
                .init(name: Parameter.tenantpublickey, value: publicKey),
                .init(name: Parameter.count, value: "\(count)"),
                .init(name: Parameter.keyid, value: keyId)
            ],
            authenticated: true
        )!

        return networkAdapter
            .perform(request: request, responseType: Response.self)
            .map(\.sessionId)
            .eraseToAnyPublisher()
    }
    
    // MARK: - KeyGenWalletAttributeClientAPI
    
    func createWalletAttributeForKeyId(
        _ keyId: String,
        userId: String
    ) -> AnyPublisher<Void, NetworkError> {
        struct Body: Encodable {
            let keyId: String
        }
        
        let body = Body(keyId: keyId)
        
        let request = requestBuilder.post(
            path: Path.makePathWithUserId(userId, path: .attribute),
            body: try? JSONEncoder().encode(body),
            headers: [HttpHeaderField.accept: HttpHeaderValue.json],
            authenticated: true
        )!
        
        return networkAdapter
            .perform(request: request)
            .eraseToAnyPublisher()
    }
    
    // MARK: - MessageSigningClientAPI
    
    func getPartialSignatureWithUserId(
        _ userId: String,
        presignatureId: String,
        message: String,
        keyId: String,
        keyType: String,
        protocolId: String
    ) -> AnyPublisher<PartialSignWithPresigResponse, NetworkError> {
        let request = requestBuilder.get(
            path: Path.makePathWithUserId(userId, path: .partialSignatures),
            parameters: [
                .init(name: Parameter.keyid, value: keyId),
                .init(name: Parameter.keyType, value: keyType),
//                .init(name: Parameter.presigid, value: presignatureId),
                .init(name: Parameter.message, value: message),
                .init(name: Parameter.protocolId, value: protocolId)
            ],
            authenticated: true
        )!

        return networkAdapter
            .perform(request: request)
            .eraseToAnyPublisher()
    }
    
    func fetchSignedTransactionHex(
        _ userId: String,
        unsignedTransaction: UnsignedTransaction
    ) -> AnyPublisher<String, NetworkError> {

        struct Response: Decodable {
            let signed_transaction_hex: String
        }
        
        let request = requestBuilder.post(
            path: Path.makePathWithUserId(userId, path: .generatesignedtxhex),
            body: try? JSONEncoder().encode(unsignedTransaction),
            authenticated: true
        )!

        return networkAdapter
            .perform(request: request, responseType: Response.self)
            .map(\.signed_transaction_hex)
            .eraseToAnyPublisher()
    }
    
    // MARK: - RecoveryClientAPI
    
    func fetchRecoveryInfoForUserId(
        _ userId: String,
        keyId: String,
        keyType: Curve,
        publicKey: String,
        tenantPublicKey: String,
        label: String,
        sessionId: String
    ) -> AnyPublisher<RecoveryInfoResponse, NetworkError> {
        let request = requestBuilder.get(
            path: Path.makePathWithUserId(userId, path: .recoveryinfo),
            parameters: [
                .init(name: Parameter.tenantpublickey, value: tenantPublicKey),
                .init(name: Parameter.erslabel, value: label),
                .init(name: Parameter.keyType, value: keyType == .ed25519 ? "eddsa" : "ecdsa"),
                .init(name: Parameter.keyid, value: keyId),
                .init(name: Parameter.erspublickey, value: publicKey),
                .init(name: Parameter.sessionid, value: sessionId)
            ],
            authenticated: true
        )!

        return networkAdapter
            .perform(request: request)
            .eraseToAnyPublisher()
    }
}
