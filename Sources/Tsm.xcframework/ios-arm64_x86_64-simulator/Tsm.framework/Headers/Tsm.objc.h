// Objective-C API for talking to go.blockdaemon.com/sepior/go-mpc-lib/app/mobile/tsm Go package.
//   gobind -lang=objc go.blockdaemon.com/sepior/go-mpc-lib/app/mobile/tsm
//
// File is generated by gobind. Do not edit.

#ifndef __Tsm_H__
#define __Tsm_H__

@import Foundation;
#include "ref.h"
#include "Universe.objc.h"


@class TsmBIP32KeyExportData;
@class TsmBIP32KeyInfo;
@class TsmChainPath;
@class TsmECDSAClient;
@class TsmECDSAKey;
@class TsmEDDSAClient;
@class TsmEdDSAKey;
@class TsmIntList;
@class TsmKeyClient;
@class TsmKeyExportData;
@class TsmPartialSignatureWithPublicKey;
@class TsmPartialSignatureWithPublicKeyWithPresigID;
@class TsmSignatureWithRecoveryID;
@class TsmTenantClient;
@class TsmWrappingClient;
@class TsmWrappingKey;
@protocol TsmExternalStorageEncryptor;
@class TsmExternalStorageEncryptor;

@protocol TsmExternalStorageEncryptor <NSObject>
- (NSData* _Nullable)authenticate:(NSData* _Nullable)data error:(NSError* _Nullable* _Nullable)error;
- (NSData* _Nullable)decrypt:(NSData* _Nullable)ciphertext identifiers:(NSData* _Nullable)identifiers error:(NSError* _Nullable* _Nullable)error;
- (NSData* _Nullable)encrypt:(NSData* _Nullable)plaintext identifiers:(NSData* _Nullable)identifiers error:(NSError* _Nullable* _Nullable)error;
@end

@interface TsmBIP32KeyExportData : NSObject <goSeqRefInterface> {
}
@property(strong, readonly) _Nonnull id _ref;

- (nonnull instancetype)initWithRef:(_Nonnull id)ref;
- (nonnull instancetype)init;
@property (nonatomic) NSData* _Nullable seedShare;
@property (nonatomic) NSData* _Nullable seedWitness;
@end

@interface TsmBIP32KeyInfo : NSObject <goSeqRefInterface> {
}
@property(strong, readonly) _Nonnull id _ref;

- (nonnull instancetype)initWithRef:(_Nonnull id)ref;
- (nonnull instancetype)init;
@property (nonatomic) NSString* _Nonnull keyType;
@property (nonatomic) TsmChainPath* _Nullable chainPath;
@property (nonatomic) NSString* _Nonnull parentKeyID;
@end

@interface TsmChainPath : NSObject <goSeqRefInterface> {
}
@property(strong, readonly) _Nonnull id _ref;

- (nonnull instancetype)initWithRef:(_Nonnull id)ref;
- (nullable instancetype)init:(long)size;
- (BOOL)get:(long)index ret0_:(long* _Nullable)ret0_ error:(NSError* _Nullable* _Nullable)error;
- (long)length;
- (BOOL)set:(long)index value:(long)value error:(NSError* _Nullable* _Nullable)error;
@end

@interface TsmECDSAClient : NSObject <goSeqRefInterface> {
}
@property(strong, readonly) _Nonnull id _ref;

- (nonnull instancetype)initWithRef:(_Nonnull id)ref;
- (nullable instancetype)init:(TsmTenantClient* _Nullable)client;
/**
 * BIP32ConvertKey converts a BIP32 key to a secp256k1 key that can be used for signing.
 */
- (NSString* _Nonnull)biP32ConvertKey:(NSString* _Nullable)sessionID bip32KeyID:(NSString* _Nullable)bip32KeyID error:(NSError* _Nullable* _Nullable)error;
/**
 * BIP32DeriveFromKey derives a BIP32 child key from a parent key according to the given chain path element.
The chain path element must specify a hard derivation, i.e., its most significant bit must be set.
 */
- (NSString* _Nonnull)biP32DeriveFromKey:(NSString* _Nullable)sessionID parentKeyID:(NSString* _Nullable)parentKeyID chainPathElement:(long)chainPathElement error:(NSError* _Nullable* _Nullable)error;
/**
 * BIP32DeriveFromSeed derives a master key and master chain code from a BIP32 seed.
 */
- (NSString* _Nonnull)biP32DeriveFromSeed:(NSString* _Nullable)sessionID seedID:(NSString* _Nullable)seedID error:(NSError* _Nullable* _Nullable)error;
/**
 * BIP32ExportSeed exports encrypted BIP32 seed shares from the TSM.
The seed is split into a number of exclusive or (xor) shares and each MPC node returns one encrypted share.
The shares are between 16 and 64 bytes, depending on the length of the seed.
Each MPC node also return a witness of the seed, computed as witness := sha512.Sum512(append([]byte("Exported Share"), seed...))
The wrapping key is supplied as a SubjectPublicKeyInfo, in ASN.1 DER encoding.
 */
- (TsmBIP32KeyExportData* _Nullable)biP32ExportSeed:(NSString* _Nullable)sessionID seedID:(NSString* _Nullable)seedID wrappingKey:(NSData* _Nullable)wrappingKey error:(NSError* _Nullable* _Nullable)error;
/**
 * BIP32GenerateSeed generates a new BIP32 seed.
 */
- (NSString* _Nonnull)biP32GenerateSeed:(NSString* _Nullable)sessionID error:(NSError* _Nullable* _Nullable)error;
/**
 * BIP32ImportSeed imports a BIP32 seed into the TSM.
The seed is computed as the exclusive or (xor) of the seed shares provided to each MPC node.
The shares must be byte arrays of length between 16 and 64 (32 is advised).
The witness is optional: If all MPC nodes receive witness=nil, no check is applied to the imported seed.
If witness is provided, the import operation is rejected unless all MPC nodes receive the same witness
and witness := sha512.Sum512(append([]byte("Exported Share"), seed...))
 */
- (NSString* _Nonnull)biP32ImportSeed:(NSString* _Nullable)sessionID seedShare:(NSData* _Nullable)seedShare witness:(NSData* _Nullable)witness error:(NSError* _Nullable* _Nullable)error;
/**
 * BIP32Info returns info about a key or seed.
The keyType is one of these: BIP32Seed, BIP32Key, ECKey.
The chainPath is nil except for BIP32Key where it is the chain path for the given key (nil if master key)
The parentKeyID is nil for BIP32Seed, and for BIP32Key it points either to the seed or the parent BIP32Key.
For an ECKey, parentKeyID is the keyID of the BIP32Key from which the key was converted.
 */
- (TsmBIP32KeyInfo* _Nullable)biP32Info:(NSString* _Nullable)keyID error:(NSError* _Nullable* _Nullable)error;
- (NSData* _Nullable)backupShare:(NSString* _Nullable)keyID error:(NSError* _Nullable* _Nullable)error;
- (NSData* _Nullable)combine:(NSData* _Nullable)partialSignatureA partialSignatureB:(NSData* _Nullable)partialSignatureB error:(NSError* _Nullable* _Nullable)error;
- (TsmKeyExportData* _Nullable)exportWrappedKeySharesWithSessionID:(NSString* _Nullable)sessionID keyID:(NSString* _Nullable)keyID wrappingPublicKey:(NSData* _Nullable)wrappingPublicKey error:(NSError* _Nullable* _Nullable)error;
- (TsmSignatureWithRecoveryID* _Nullable)finalize:(NSData* _Nullable)partialSignature error:(NSError* _Nullable* _Nullable)error;
- (NSData* _Nullable)getTenantPublicKey;
- (NSString* _Nonnull)importWrappedKeySharesWithSessionID:(NSString* _Nullable)sessionID curveName:(NSString* _Nullable)curveName wrappedKeyShare:(NSData* _Nullable)wrappedKeyShare wrappedChainCode:(NSData* _Nullable)wrappedChainCode derPublicKey:(NSData* _Nullable)derPublicKey keyID:(NSString* _Nullable)keyID error:(NSError* _Nullable* _Nullable)error;
- (NSString* _Nonnull)keygenWithSessionAndKeyID:(NSString* _Nullable)sessionID requestedKeyID:(NSString* _Nullable)requestedKeyID curveName:(NSString* _Nullable)curveName error:(NSError* _Nullable* _Nullable)error;
- (NSString* _Nonnull)keygenWithSessionID:(NSString* _Nullable)sessionID curveName:(NSString* _Nullable)curveName error:(NSError* _Nullable* _Nullable)error;
- (NSData* _Nullable)partialRecoveryInfo:(NSString* _Nullable)sessionID keyID:(NSString* _Nullable)keyID ersPublicKey:(NSData* _Nullable)ersPublicKey label:(NSData* _Nullable)label error:(NSError* _Nullable* _Nullable)error;
- (NSData* _Nullable)partialSign:(NSString* _Nullable)sessionID keyID:(NSString* _Nullable)keyID chainPath:(TsmChainPath* _Nullable)chainPath messageHash:(NSData* _Nullable)messageHash error:(NSError* _Nullable* _Nullable)error;
- (NSData* _Nullable)partialSignWithPlayers:(NSString* _Nullable)sessionID keyID:(NSString* _Nullable)keyID chainPath:(TsmChainPath* _Nullable)chainPath messageHash:(NSData* _Nullable)messageHash players:(TsmIntList* _Nullable)players error:(NSError* _Nullable* _Nullable)error;
- (TsmPartialSignatureWithPublicKeyWithPresigID* _Nullable)partialSignWithPresig:(NSString* _Nullable)keyID presigID:(NSString* _Nullable)presigID chainPath:(TsmChainPath* _Nullable)chainPath messageHash:(NSData* _Nullable)messageHash error:(NSError* _Nullable* _Nullable)error;
- (NSString* _Nonnull)presigGenWithSessionID:(NSString* _Nullable)sessionID keyID:(NSString* _Nullable)keyID count:(long)count error:(NSError* _Nullable* _Nullable)error;
- (NSData* _Nullable)publicKey:(NSString* _Nullable)keyID chainPath:(TsmChainPath* _Nullable)chainPath error:(NSError* _Nullable* _Nullable)error;
- (NSData* _Nullable)recoveryInfo:(NSString* _Nullable)sessionID keyID:(NSString* _Nullable)keyID ersPublicKey:(NSData* _Nullable)ersPublicKey label:(NSData* _Nullable)label error:(NSError* _Nullable* _Nullable)error;
- (BOOL)registerTenantPublicKey:(NSString* _Nullable)sessionID tenantPublicKey:(NSData* _Nullable)tenantPublicKey error:(NSError* _Nullable* _Nullable)error;
- (BOOL)reshare:(NSString* _Nullable)sessionID keyID:(NSString* _Nullable)keyID error:(NSError* _Nullable* _Nullable)error;
- (NSString* _Nonnull)restoreShare:(NSData* _Nullable)shareBackup error:(NSError* _Nullable* _Nullable)error;
- (BOOL)verify:(NSData* _Nullable)derPublicKey messageHash:(NSData* _Nullable)messageHash derSignature:(NSData* _Nullable)derSignature error:(NSError* _Nullable* _Nullable)error;
@end

@interface TsmECDSAKey : NSObject <goSeqRefInterface> {
}
@property(strong, readonly) _Nonnull id _ref;

- (nonnull instancetype)initWithRef:(_Nonnull id)ref;
- (nonnull instancetype)init;
@property (nonatomic) NSString* _Nonnull curveName;
@property (nonatomic) NSData* _Nullable privateKey;
@property (nonatomic) NSData* _Nullable masterChainCode;
@end

@interface TsmEDDSAClient : NSObject <goSeqRefInterface> {
}
@property(strong, readonly) _Nonnull id _ref;

- (nonnull instancetype)initWithRef:(_Nonnull id)ref;
- (nullable instancetype)init:(TsmTenantClient* _Nullable)client;
- (NSData* _Nullable)backupShare:(NSString* _Nullable)keyID error:(NSError* _Nullable* _Nullable)error;
- (NSData* _Nullable)combine:(NSData* _Nullable)partialSignatureA partialSignatureB:(NSData* _Nullable)partialSignatureB error:(NSError* _Nullable* _Nullable)error;
- (TsmKeyExportData* _Nullable)exportWrappedKeySharesWithSessionID:(NSString* _Nullable)sessionID keyID:(NSString* _Nullable)keyID wrappingPublicKey:(NSData* _Nullable)wrappingPublicKey error:(NSError* _Nullable* _Nullable)error;
- (NSData* _Nullable)finalize:(NSData* _Nullable)partialSignature error:(NSError* _Nullable* _Nullable)error;
- (NSData* _Nullable)getTenantPublicKey;
- (NSString* _Nonnull)importWrappedKeySharesWithSessionID:(NSString* _Nullable)sessionID curveName:(NSString* _Nullable)curveName wrappedKeyShare:(NSData* _Nullable)wrappedKeyShare wrappedChainCode:(NSData* _Nullable)wrappedChainCode derPublicKey:(NSData* _Nullable)derPublicKey keyID:(NSString* _Nullable)keyID error:(NSError* _Nullable* _Nullable)error;
- (NSString* _Nonnull)keygenWithSessionAndKeyID:(NSString* _Nullable)sessionID requestedKeyID:(NSString* _Nullable)requestedKeyID curveName:(NSString* _Nullable)curveName error:(NSError* _Nullable* _Nullable)error;
- (NSString* _Nonnull)keygenWithSessionID:(NSString* _Nullable)sessionID curveName:(NSString* _Nullable)curveName error:(NSError* _Nullable* _Nullable)error;
- (NSData* _Nullable)partialRecoveryInfo:(NSString* _Nullable)sessionID keyID:(NSString* _Nullable)keyID ersPublicKey:(NSData* _Nullable)ersPublicKey label:(NSData* _Nullable)label error:(NSError* _Nullable* _Nullable)error;
- (NSData* _Nullable)partialSign:(NSString* _Nullable)sessionID keyID:(NSString* _Nullable)keyID chainPath:(TsmChainPath* _Nullable)chainPath message:(NSData* _Nullable)message error:(NSError* _Nullable* _Nullable)error;
- (NSData* _Nullable)partialSignWithPlayers:(NSString* _Nullable)sessionID keyID:(NSString* _Nullable)keyID chainPath:(TsmChainPath* _Nullable)chainPath messageHash:(NSData* _Nullable)messageHash players:(TsmIntList* _Nullable)players error:(NSError* _Nullable* _Nullable)error;
- (TsmPartialSignatureWithPublicKeyWithPresigID* _Nullable)partialSignWithPresig:(NSString* _Nullable)keyID presigID:(NSString* _Nullable)presigID chainPath:(TsmChainPath* _Nullable)chainPath messageHash:(NSData* _Nullable)messageHash error:(NSError* _Nullable* _Nullable)error;
- (NSString* _Nonnull)presigGenWithSessionID:(NSString* _Nullable)sessionID keyID:(NSString* _Nullable)keyID count:(long)count error:(NSError* _Nullable* _Nullable)error;
- (NSData* _Nullable)publicKey:(NSString* _Nullable)keyID chainPath:(TsmChainPath* _Nullable)chainPath error:(NSError* _Nullable* _Nullable)error;
- (BOOL)registerTenantPublicKey:(NSString* _Nullable)sessionID tenantPublicKey:(NSData* _Nullable)tenantPublicKey error:(NSError* _Nullable* _Nullable)error;
- (NSString* _Nonnull)restoreShare:(NSData* _Nullable)shareBackup error:(NSError* _Nullable* _Nullable)error;
- (BOOL)verify25519:(NSData* _Nullable)publicKey message:(NSData* _Nullable)message signature:(NSData* _Nullable)signature error:(NSError* _Nullable* _Nullable)error;
- (BOOL)verify448:(NSData* _Nullable)publicKey message:(NSData* _Nullable)message signature:(NSData* _Nullable)signature error:(NSError* _Nullable* _Nullable)error;
@end

@interface TsmEdDSAKey : NSObject <goSeqRefInterface> {
}
@property(strong, readonly) _Nonnull id _ref;

- (nonnull instancetype)initWithRef:(_Nonnull id)ref;
- (nonnull instancetype)init;
@property (nonatomic) NSString* _Nonnull curveName;
@property (nonatomic) NSData* _Nullable privateKey;
@property (nonatomic) NSData* _Nullable masterChainCode;
@end

@interface TsmIntList : NSObject <goSeqRefInterface> {
}
@property(strong, readonly) _Nonnull id _ref;

- (nonnull instancetype)initWithRef:(_Nonnull id)ref;
- (nullable instancetype)init:(long)size;
- (BOOL)set:(long)index value:(long)value error:(NSError* _Nullable* _Nullable)error;
@end

@interface TsmKeyClient : NSObject <goSeqRefInterface> {
}
@property(strong, readonly) _Nonnull id _ref;

- (nonnull instancetype)initWithRef:(_Nonnull id)ref;
- (nullable instancetype)init:(TsmTenantClient* _Nullable)client;
- (BOOL)countPresigsForKey:(NSString* _Nullable)keyID ret0_:(long* _Nullable)ret0_ error:(NSError* _Nullable* _Nullable)error;
- (BOOL)delete:(NSString* _Nullable)keyID error:(NSError* _Nullable* _Nullable)error;
- (BOOL)deletePresigsForKey:(NSString* _Nullable)keyID error:(NSError* _Nullable* _Nullable)error;
/**
 * GetKeysForUser returns the list of keyIDs for the logged on user in a single string with , separation.
 */
- (NSString* _Nonnull)getKeysForUser:(NSError* _Nullable* _Nullable)error;
@end

@interface TsmKeyExportData : NSObject <goSeqRefInterface> {
}
@property(strong, readonly) _Nonnull id _ref;

- (nonnull instancetype)initWithRef:(_Nonnull id)ref;
- (nonnull instancetype)init;
@property (nonatomic) NSData* _Nullable keyShare;
@property (nonatomic) NSData* _Nullable chainCode;
@property (nonatomic) NSString* _Nonnull curve;
@property (nonatomic) NSData* _Nullable publicKey;
@end

@interface TsmPartialSignatureWithPublicKey : NSObject <goSeqRefInterface> {
}
@property(strong, readonly) _Nonnull id _ref;

- (nonnull instancetype)initWithRef:(_Nonnull id)ref;
- (nonnull instancetype)init;
@property (nonatomic) NSData* _Nullable partialSignature;
@property (nonatomic) NSData* _Nullable publicKey;
@end

@interface TsmPartialSignatureWithPublicKeyWithPresigID : NSObject <goSeqRefInterface> {
}
@property(strong, readonly) _Nonnull id _ref;

- (nonnull instancetype)initWithRef:(_Nonnull id)ref;
- (nonnull instancetype)init;
@property (nonatomic) NSData* _Nullable partialSignature;
@property (nonatomic) NSData* _Nullable publicKey;
@property (nonatomic) NSString* _Nonnull presigID;
@end

@interface TsmSignatureWithRecoveryID : NSObject <goSeqRefInterface> {
}
@property(strong, readonly) _Nonnull id _ref;

- (nonnull instancetype)initWithRef:(_Nonnull id)ref;
- (nonnull instancetype)init;
@property (nonatomic) NSData* _Nullable signature;
@property (nonatomic) long recoveryID;
@end

@interface TsmTenantClient : NSObject <goSeqRefInterface> {
}
@property(strong, readonly) _Nonnull id _ref;

- (nonnull instancetype)initWithRef:(_Nonnull id)ref;
- (nonnull instancetype)init;
- (NSString* _Nonnull)getTenantPublicKey;
- (void)stop;
@end

@interface TsmWrappingClient : NSObject <goSeqRefInterface> {
}
@property(strong, readonly) _Nonnull id _ref;

- (nonnull instancetype)initWithRef:(_Nonnull id)ref;
- (nullable instancetype)init:(TsmTenantClient* _Nullable)client;
- (TsmWrappingKey* _Nullable)wrappingKeys:(NSError* _Nullable* _Nullable)error;
@end

@interface TsmWrappingKey : NSObject <goSeqRefInterface> {
}
@property(strong, readonly) _Nonnull id _ref;

- (nonnull instancetype)initWithRef:(_Nonnull id)ref;
- (nonnull instancetype)init;
@property (nonatomic) NSData* _Nullable subjectPublicKeyInfo;
@property (nonatomic) NSString* _Nonnull fingerprint;
@end

/**
 * CombinePartialRecoveryInfos must be done for every partial recovery info
before giving the result to FinalizeRecoveryInfo.
 */
FOUNDATION_EXPORT NSData* _Nullable TsmCombinePartialRecoveryInfos(NSData* _Nullable pri1, NSData* _Nullable pri2, NSError* _Nullable* _Nullable error);

/**
 * FinalizeRecoveryInfo takes a combined array of partial recovery infos and creates a single recovery info.
 */
FOUNDATION_EXPORT NSData* _Nullable TsmFinalizeRecoveryInfo(NSData* _Nullable pris, NSData* _Nullable ersPublicKey, NSData* _Nullable ersLabel, NSError* _Nullable* _Nullable error);

FOUNDATION_EXPORT NSString* _Nonnull TsmGenerateSessionID(void);

FOUNDATION_EXPORT TsmChainPath* _Nullable TsmNewChainPath(long size);

FOUNDATION_EXPORT TsmECDSAClient* _Nullable TsmNewECDSAClient(TsmTenantClient* _Nullable client);

FOUNDATION_EXPORT TsmEDDSAClient* _Nullable TsmNewEDDSAClient(TsmTenantClient* _Nullable client);

FOUNDATION_EXPORT TsmTenantClient* _Nullable TsmNewEmbeddedClient(long n, long t, NSString* _Nullable configuration, NSString* _Nullable logConfiguration, NSError* _Nullable* _Nullable error);

FOUNDATION_EXPORT TsmTenantClient* _Nullable TsmNewEmbeddedClientWithCustomEncryptor(long n, long t, NSString* _Nullable configuration, NSString* _Nullable logConfiguration, id<TsmExternalStorageEncryptor> _Nullable encryptor, NSError* _Nullable* _Nullable error);

FOUNDATION_EXPORT TsmTenantClient* _Nullable TsmNewEmbeddedTenantClient(long n, long t, NSString* _Nullable configuration, NSString* _Nullable logConfiguration, NSError* _Nullable* _Nullable error);

FOUNDATION_EXPORT TsmTenantClient* _Nullable TsmNewEmbeddedTenantClientWithCustomEncryptor(long n, long t, NSString* _Nullable configuration, NSString* _Nullable logConfiguration, id<TsmExternalStorageEncryptor> _Nullable encryptor, NSError* _Nullable* _Nullable error);

FOUNDATION_EXPORT TsmIntList* _Nullable TsmNewIntList(long size);

FOUNDATION_EXPORT TsmKeyClient* _Nullable TsmNewKeyClient(TsmTenantClient* _Nullable client);

FOUNDATION_EXPORT TsmTenantClient* _Nullable TsmNewPasswordClientFromEncoding(long n, long t, NSString* _Nullable encoding, NSError* _Nullable* _Nullable error);

FOUNDATION_EXPORT TsmWrappingClient* _Nullable TsmNewWrappingClient(TsmTenantClient* _Nullable client);

/**
 * RecoverKeyECDSA takes a final recovery info and returns an object with the private key in ASN1 format.
 */
FOUNDATION_EXPORT TsmECDSAKey* _Nullable TsmRecoverKeyECDSA(NSData* _Nullable recoveryInfo, NSData* _Nullable ersPrivateKeyBytes, NSData* _Nullable ersLabel, NSError* _Nullable* _Nullable error);

/**
 * RecoverKeyEdDSA takes a final recovery info and returns an object with the private key in ASN1 format.
 */
FOUNDATION_EXPORT TsmEdDSAKey* _Nullable TsmRecoverKeyEdDSA(NSData* _Nullable recoveryInfo, NSData* _Nullable ersPrivateKeyBytes, NSData* _Nullable ersLabel, NSError* _Nullable* _Nullable error);

/**
 * ValidateRecoveryInfo validates that the final recovery info correctly restores to a private key matching the given public key.
 */
FOUNDATION_EXPORT BOOL TsmValidateRecoveryInfo(NSData* _Nullable recoveryInfo, NSData* _Nullable ersPublicKeyBytes, NSData* _Nullable ersLabel, NSData* _Nullable publicKey, NSError* _Nullable* _Nullable error);

@class TsmExternalStorageEncryptor;

@interface TsmExternalStorageEncryptor : NSObject <goSeqRefInterface, TsmExternalStorageEncryptor> {
}
@property(strong, readonly) _Nonnull id _ref;

- (nonnull instancetype)initWithRef:(_Nonnull id)ref;
- (NSData* _Nullable)authenticate:(NSData* _Nullable)data error:(NSError* _Nullable* _Nullable)error;
- (NSData* _Nullable)decrypt:(NSData* _Nullable)ciphertext identifiers:(NSData* _Nullable)identifiers error:(NSError* _Nullable* _Nullable)error;
- (NSData* _Nullable)encrypt:(NSData* _Nullable)plaintext identifiers:(NSData* _Nullable)identifiers error:(NSError* _Nullable* _Nullable)error;
@end

#endif
