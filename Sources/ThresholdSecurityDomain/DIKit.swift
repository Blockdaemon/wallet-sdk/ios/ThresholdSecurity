// Copyright © Blockdaemon All rights reserved.

import DIKit

public enum DIKitContext: String {
    case ecdsa
    case eddsa
}

extension DependencyContainer {
    
    // MARK: - ThresholdSecurityDomain
    
    public static var thresholdSecurityDomain = module {
        
        factory { SessionService() as SessionServiceAPI }
        
        factory { KeyGenRepositoryProvider() as KeyGenRepositoryProviding }
        
        factory { RecoveryRepositoryProvider() as RecoveryRepositoryProviding }
        
        factory { TSMGlobalService() as TSMGlobalServiceAPI }
        
        factory { MessageSigningService() as MessageSigningServiceAPI }
        
        single { KeyIdDirectory() as KeyIdDirectoryAPI }
    }
}
