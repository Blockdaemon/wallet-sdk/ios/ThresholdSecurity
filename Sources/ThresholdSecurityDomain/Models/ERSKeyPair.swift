// Copyright © Blockdaemon All rights reserved.

import Foundation

public struct ERSKeyPair {
    public let publicKey: Data
    public let encryptedPrivateKey: Data
    public let password: String
    
    public init(publicKey: Data, encryptedPrivateKey: Data, password: String) {
        self.publicKey = publicKey
        self.encryptedPrivateKey = encryptedPrivateKey
        self.password = password
    }
}
