// Copyright © Blockdaemon All rights reserved.

import CommonCryptoKit
import Foundation

public struct KeyGen: Codable, Equatable {
    public var curve: Curve {
        keyId.curve
    }
    
    public var identifier: String {
        keyId.identifier
    }
    
    public let keyId: KeyId
    public let keyshare: Data
    
    public init(
        keyId: KeyId,
        keyshare: Data
    ) {
        self.keyId = keyId
        self.keyshare = keyshare
    }
}

extension KeyGen {
    public static let ed25519: KeyGen = .init(keyId: .init(curve: .ed25519, identifier: "1234"), keyshare: Data())
}
