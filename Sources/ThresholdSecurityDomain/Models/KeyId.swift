// Copyright © Blockdaemon All rights reserved.

import Foundation
import CommonCryptoKit
import ToolKit

public class KeyId: NSObject, NSSecureCoding, Codable {
    public static var supportsSecureCoding: Bool = true

    public let curve: Curve
    public let identifier: String
    
    public init(curve: Curve, identifier: String) {
        self.curve = curve
        self.identifier = identifier
    }
    
    public func encode(with coder: NSCoder) {
        coder.encode(curve.rawValue, forKey: "curve")
        coder.encode(identifier, forKey: "identifier")
    }
    
    public required init?(coder: NSCoder) {
        guard let raw = coder.decodeObject(of: NSString.self, forKey: "curve") as? String else { return nil }
        guard let curve = Curve(rawValue: raw) else { return nil }
        guard let identifier = coder.decodeObject(of: NSString.self, forKey: "identifier") as? String else { return nil }
        self.curve = curve
        self.identifier = identifier
    }
}

extension KeyId {
    public static func == (lhs: KeyId, rhs: KeyId) -> Bool {
        lhs.curve == rhs.curve &&
        lhs.identifier == rhs.identifier
    }
}

class KeyUserIdWrapper: NSObject, NSSecureCoding {
    static var supportsSecureCoding: Bool = true
    
    let userId: String
    var keyIds: [KeyId]
    
    init(userId: String, keyIds: [KeyId]) {
        self.userId = userId
        self.keyIds = keyIds
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(userId, forKey: "userId")
        coder.encode(keyIds, forKey: "keyIds")
    }
    
    required init?(coder: NSCoder) {
        guard let userId = coder.decodeObject(of: NSString.self, forKey: "userId") as? String else { return nil }
        guard let keyIds = coder.decodeArrayOfObjects(ofClass: KeyId.self, forKey: "keyIds") else { return nil }
        self.userId = userId
        self.keyIds = keyIds
    }
}
