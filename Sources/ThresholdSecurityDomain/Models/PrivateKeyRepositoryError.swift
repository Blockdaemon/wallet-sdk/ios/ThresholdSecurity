// Copyright © Blockdaemon All rights reserved.

import Foundation

public enum PrivateKeyRepositoryError: Error, CustomStringConvertible {
    case keychainError(String)
    case invalidSymmetricKey(Error)
    case itemNotFound
    case duplicateItem
    case interactionNotAllowed
    case decodeError
    case temporaryPasswordCreationFailed
    case authenticationFailed
    case cloudKit(Error)
    case unknown
    
    public var description: String {
        switch self {
        case .keychainError(let message):
            return "Keychain Error: \(message)"
        case .itemNotFound:
            return "Keychain Item Not Found"
        case .duplicateItem:
            return "Duplicate Keychain Item"
        case .interactionNotAllowed:
            return "Keychain Interaction Not Allowed"
        case .decodeError:
            return "Keychain Decode Error"
        case .authenticationFailed:
            return "Keychain Authentication Failed"
        case .cloudKit(let error):
            return "CloudKit Error: \(error.localizedDescription)"
        case .invalidSymmetricKey(let error):
            return "Invalid SymmetricKey Error: \(error.localizedDescription)"
        case .temporaryPasswordCreationFailed:
            return "Temporary password creation failed"
        case .unknown:
            return "Unknown Keychain Error"
        }
    }
}
