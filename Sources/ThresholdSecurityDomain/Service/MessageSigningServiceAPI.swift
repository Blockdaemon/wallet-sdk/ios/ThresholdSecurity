// Copyright © Blockdaemon All rights reserved.

import Combine
import CommonCryptoKit
import DIKit
import Errors
import Extensions

public enum MessageSigningServiceError: Error {
    case provider(MessageSigningProviderError)
    case repository(MessageSigningRepositoryError)
}

public protocol MessageSigningServiceAPI {
    
    func generateSigningInputsFromTransactionHash(
        _ transactionHash: String,
        userId: String,
        signingInputs: [SigningInput],
        keyId: String,
        chainMetadata: ChainMetadata
    ) -> AnyPublisher<[SignedInputs], MessageSigningServiceError>
}

final class MessageSigningService: MessageSigningServiceAPI {
    
    private let provider: MessageSigningProviderAPI
    
    init(provider: MessageSigningProviderAPI = resolve()) {
        self.provider = provider
    }
    
    func generateSigningInputsFromTransactionHash(
        _ transactionHash: String,
        userId: String,
        signingInputs: [SigningInput],
        keyId: String,
        chainMetadata: ChainMetadata
    ) -> AnyPublisher<[SignedInputs], MessageSigningServiceError> {
        provider.signingRepositoryWithChainMetadata(
            chainMetadata,
            keyId: keyId
        )
        .mapError(MessageSigningServiceError.provider)
        .flatMap { repository in
            repository
                .generatedSignedInputsFromTransactionHash(
                    transactionHash,
                    inputs: signingInputs,
                    userId: userId
                )
                .mapError(MessageSigningServiceError.repository)
        }
        .eraseToAnyPublisher()
    }
}
