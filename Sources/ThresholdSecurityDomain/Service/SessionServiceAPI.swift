// Copyright © Blockdaemon All rights reserved.

import Combine
import CommonCryptoKit
import DIKit
import Errors
import Extensions

public protocol SessionServiceAPI {
    func generateKeygenSessionIdForUserId(
        _ userId: String,
        curve: Curve
    ) -> AnyPublisher<String, SessionServiceError>
}

public enum SessionServiceError: Error {
    case repository(SessionRepositoryError)
}

final class SessionService: SessionServiceAPI {
    
    private let repository: SessionRepositoryAPI
    
    init(repository: SessionRepositoryAPI = resolve()) {
        self.repository = repository
    }
    
    func generateKeygenSessionIdForUserId(
        _ userId: String,
        curve: Curve
    ) -> AnyPublisher<String, SessionServiceError> {
        repository
            .generateKeygenSessionIdForUserId(
                userId,
                curve: curve
            )
            .mapError(SessionServiceError.repository)
            .eraseToAnyPublisher()
    }
    
}
