// Copyright © Blockdaemon All rights reserved.

import CommonCryptoKit
import Combine
import DIKit
import Errors
import Extensions

protocol PresignatureServiceAPI {
    func generatePresignatureSessionIdForUserId(
        _ userId: String,
        publicKey: String,
        keyId: String,
        curveType: Curve
    ) -> AnyPublisher<String, PresignatureServiceError>
}

enum PresignatureServiceError: Error {
    case repository(PresignatureRepositoryError)
}

final class PresignatureService: PresignatureServiceAPI {
    
    private let repository: PresignatureRepositoryAPI
    
    init(repository: PresignatureRepositoryAPI = resolve()) {
        self.repository = repository
    }
    
    func generatePresignatureSessionIdForUserId(
        _ userId: String,
        publicKey: String,
        keyId: String,
        curveType: Curve
    ) -> AnyPublisher<String, PresignatureServiceError> {
        repository
            .generatePresignatureSessionIdForUserId(
                userId,
                publicKey: publicKey,
                keyId: keyId,
                curveType: curveType
            )
            .mapError(PresignatureServiceError.repository)
            .eraseToAnyPublisher()
    }
    
}
