// Copyright © Blockdaemon All rights reserved.

import Combine
import Foundation

public protocol PrivateKeyRepositoryAPI {

    /// Returns a temporary password used for wallet backup/recovery
    var temporaryPassword: AnyPublisher<Data, PrivateKeyRepositoryError> { get }
    
    /// Deletes privateKey. Only works for debug builds.
    func deleteEncryptionKeyForUserId(_ userId: String) -> AnyPublisher<Void, PrivateKeyRepositoryError>
    
    /// Creates or fetches Encryption Key data. Use extension on SymmetricKey in `Extensions` module to create SymmetricKey
    func createOrFetchEncryptionKeyDataForUserId(
        _ userId: String
    ) -> AnyPublisher<Data, PrivateKeyRepositoryError>
    
    /// Fetches Encryption Key data. Use extension on SymmetricKey in `Extensions` module to create SymmetricKey
    func fetchEncryptionKeyDataForUserId(
        _ userId: String
    ) -> AnyPublisher<Data, PrivateKeyRepositoryError>
    
    /// Fetches Encryption Key data from Cloud. Use extension on SymmetricKey in `Extensions` module to create SymmetricKey
    func fetchRemoteEncryptionKeyDataForUserId(
        _ userId: String
    ) -> AnyPublisher<Data?, PrivateKeyRepositoryError>
}
