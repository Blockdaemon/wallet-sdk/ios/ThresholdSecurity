// Copyright © Blockdaemon All rights reserved.

import Combine
import CommonCryptoKit
import DIKit

public protocol KeyGenRepositoryProviding {
    func make(with curve: Curve) -> KeyGenRepositoryAPI
}

final class KeyGenRepositoryProvider: KeyGenRepositoryProviding {
    func make(with curve: Curve) -> KeyGenRepositoryAPI {
        switch curve {
        case .ed25519:
            return DIKit.resolve(tag: DIKitContext.eddsa)
        case .secp256k1:
            return DIKit.resolve(tag: DIKitContext.ecdsa)
        }
    }
}
