// Copyright © Blockdaemon All rights reserved.

import Combine
import DIKit
import Errors
import Foundation

public enum KeyGenRepositoryError: Error {
    case emptyKeyId(Error)
    case emptyBackupShare
    case unknown
    case network(NetworkError)
    case tsmClientError(Error)
}

public protocol KeyGenRepositoryAPI {
    func generateKeysForSessionId(
        _ sessionId: String,
        userId: String
    ) -> AnyPublisher<KeyGen, KeyGenRepositoryError>
    
    func restoreKeysForSessionId(
        _ sessionId: String,
        backup: Data
    ) -> AnyPublisher<KeyGen, KeyGenRepositoryError>
    
    func generateBackupKeyShareForKeyId(
        _ keyId: String
    ) -> AnyPublisher<KeyGen, KeyGenRepositoryError>
}
