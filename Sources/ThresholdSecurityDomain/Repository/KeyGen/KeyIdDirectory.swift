// Copyright © Blockdaemon All rights reserved.

import Combine
import CommonCryptoKit
import Foundation
import ToolKit

public protocol KeyIdDirectoryAPI {
    subscript(userId: String, curve: Curve) -> KeyId? { get set }
    func addKeyIdsToUserId(_ userId: String, keyIds: [KeyId]) -> AnyPublisher<Void, KeyIdDirectoryError>
    func addKeyIdToUserId(_ userId: String, keyId: KeyId) -> AnyPublisher<Void, KeyIdDirectoryError>
    func doesUserIdHaveKeyIdForAllCurveTypes(_ userId: String) -> AnyPublisher<Bool, Never>
}

public enum KeyIdDirectoryError: Error {
    case saveFailed
}

public class KeyIdDirectory: KeyIdDirectoryAPI {
    
    public static let storageKey = "KeyIdDirectoryStorageKey"
    private let defaults: UserDefaults
    private var cancellables = Set<AnyCancellable>()
    private var storages: [KeyUserIdWrapper] = []
    private var userIdPublishers: [String: CurrentValueSubject<Bool, Never>] = [:]
    
    public init(defaults: UserDefaults = .standard) {
        self.defaults = defaults
        setup()
    }
    
    // MARK: - KeyIdDirectoryAPI
    
    public subscript(
        userId: String,
        curve: Curve
    ) -> KeyId? {
        get {
            storages.first { $0.userId == userId }?.keyIds.first { $0.curve == curve }
        }
        set(newKeyId) {
            if let index = storages.firstIndex(where: { $0.userId == userId }) {
                if let newValue = newKeyId {
                    var existingKeyIds = storages[index].keyIds.filter { $0.curve != curve }
                    existingKeyIds.append(newValue)
                    storages[index].keyIds = existingKeyIds
                } else {
                    storages[index].keyIds.removeAll { $0.curve == curve }
                    if storages[index].keyIds.isEmpty {
                        storages.remove(at: index)
                    }
                }
            } else if let newKeyId = newKeyId {
                let wrapper = KeyUserIdWrapper(userId: userId, keyIds: [newKeyId])
                storages.append(wrapper)
                Logger.shared.debug("Added new KeyUserIdWrapper for userId: \(userId) with keyIds: \(wrapper.keyIds)")
            }
        }
    }
    
    public func addKeyIdsToUserId(
        _ userId: String,
        keyIds: [KeyId]
    ) -> AnyPublisher<Void, KeyIdDirectoryError> {
        Future { [weak self] promise in
            guard let self = self else { fatalError("") }
            
            if let index = self.storages.firstIndex(where: { $0.userId == userId }) {
                self.storages[index].keyIds.append(contentsOf: keyIds)
            } else {
                let wrapper = KeyUserIdWrapper(userId: userId, keyIds: keyIds)
                self.storages.append(wrapper)
            }
            
            Logger.shared.debug("Adding keyIds to userId: \(userId). Total keyIds after addition: \(self.storages.first(where: { $0.userId == userId })?.keyIds.count ?? 0)")
            self.save().sink(receiveCompletion: { completion in
                if case .failure(_) = completion {
                    promise(.failure(KeyIdDirectoryError.saveFailed))
                }
            }, receiveValue: {
                let hasAllKeyIds = self.checkHasKeyIdsForAllCurveTypes(userId)
                self.userIdPublishers[userId]?.send(hasAllKeyIds)
                
                promise(.success(()))
            }).store(in: &self.cancellables)
        }.eraseToAnyPublisher()
    }
    
    public func addKeyIdToUserId(
        _ userId: String,
        keyId: KeyId
    ) -> AnyPublisher<Void, KeyIdDirectoryError> {
        addKeyIdsToUserId(userId, keyIds: [keyId])
    }
    
    public func doesUserIdHaveKeyIdForAllCurveTypes(
        _ userId: String
    ) -> AnyPublisher<Bool, Never> {
        if let publisher = userIdPublishers[userId] {
            return publisher.eraseToAnyPublisher()
        } else {
            let newPublisher = CurrentValueSubject<Bool, Never>(checkHasKeyIdsForAllCurveTypes(userId))
            userIdPublishers[userId] = newPublisher
            return newPublisher.eraseToAnyPublisher()
        }
    }
    
    // MARK: - Private Functions
    
    private func setup() {
        if let data = defaults.data(forKey: KeyIdDirectory.storageKey) {
            Logger.shared.debug("Found data for key \(KeyIdDirectory.storageKey), attempting to unarchive. Data size: \(data.count) bytes")
            do {
                guard let stored = try NSKeyedUnarchiver.unarchivedArrayOfObjects(ofClass: KeyUserIdWrapper.self, from: data) else { fatalError("Failed to decode local storage") }
                self.storages = stored
            } catch {
                Logger.shared.error("Failed to load storages: \(error)")
            }
        } else {
            Logger.shared.debug("No data found for key \(KeyIdDirectory.storageKey)")
        }
    }
    
    private func save() -> AnyPublisher<Void, KeyIdDirectoryError> {
        Future<Void, KeyIdDirectoryError> { [weak self] promise in
            guard let self = self else { fatalError() }
            do {
                let data = try NSKeyedArchiver.archivedData(withRootObject: self.storages, requiringSecureCoding: true)
                Logger.shared.debug("Data size being saved: \(data.count) bytes, hash: \(data.hashValue)")
                self.defaults.set(data, forKey: KeyIdDirectory.storageKey)
                promise(.success(()))
            } catch {
                Logger.shared.error("Failed to save storages: \(error)")
                promise(.failure(KeyIdDirectoryError.saveFailed))
            }
        }.eraseToAnyPublisher()
    }
    
    private func checkHasKeyIdsForAllCurveTypes(_ userId: String) -> Bool {
        guard let keyIds = storages.first(where: { $0.userId == userId })?.keyIds else {
            return false
        }
        return Curve.allCases.allSatisfy { curve in
            keyIds.contains { $0.curve == curve }
        }
    }
}
