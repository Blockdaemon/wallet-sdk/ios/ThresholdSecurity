// Copyright © Blockdaemon All rights reserved.

import Combine

public enum TSMGlobalRepositoryError: Error {
    case emptyTsmDatabaseForFilePath
}

public protocol TSMGlobalRepositoryAPI {
    
    /// Removes local database and resets TSM instances for all curve types
    func nuke() -> AnyPublisher<Void, TSMGlobalRepositoryError>
}
