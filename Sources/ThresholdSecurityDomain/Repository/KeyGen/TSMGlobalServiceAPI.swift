// Copyright © Blockdaemon All rights reserved.

import Combine
import DIKit

public enum TSMGlobalServiceError: Error {
    case repository(TSMGlobalRepositoryError)
}

public protocol TSMGlobalServiceAPI {

    /// Removes local database and resets TSM instances for all curve types
    func nuke() -> AnyPublisher<Void, TSMGlobalServiceError>
}

final class TSMGlobalService: TSMGlobalServiceAPI {
    
    private let repository: TSMGlobalRepositoryAPI
    
    init(repository: TSMGlobalRepositoryAPI = resolve()) {
        self.repository = repository
    }
    
    func nuke() -> AnyPublisher<Void, TSMGlobalServiceError> {
        repository
            .nuke()
            .mapError(TSMGlobalServiceError.repository)
            .eraseToAnyPublisher()
    }
}
