// Copyright © Blockdaemon All rights reserved.

import Combine
import DIKit
import Foundation
import Errors

public enum KeyGenWalletAttributeError: Error {
    case network(NetworkError)
}

public protocol KeyGenWalletAttributeRepositoryAPI {
    func createWalletAttributeForKeyId(
        _ keyId: String,
        userId: String
    ) -> AnyPublisher<Void, NetworkError>
}
