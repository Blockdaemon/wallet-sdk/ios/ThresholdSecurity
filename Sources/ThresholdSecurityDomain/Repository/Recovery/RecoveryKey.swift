// Copyright © Blockdaemon All rights reserved.

import CommonCryptoKit
import Foundation

public struct RecoveryKey {
    public let curve: Curve
    public let privateKey: Data
    public let chainCode: Data
    
    public init(curve: Curve, privateKey: Data, chainCode: Data) {
        self.curve = curve
        self.privateKey = privateKey
        self.chainCode = chainCode
    }
}
