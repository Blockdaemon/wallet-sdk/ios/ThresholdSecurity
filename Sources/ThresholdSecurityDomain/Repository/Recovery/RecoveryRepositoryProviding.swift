// Copyright © Blockdaemon All rights reserved.

import Combine
import CommonCryptoKit
import DIKit

public protocol RecoveryRepositoryProviding {
    func make(with curve: Curve) -> RecoveryRepositoryAPI
}

final class RecoveryRepositoryProvider: RecoveryRepositoryProviding {
    func make(with curve: Curve) -> RecoveryRepositoryAPI {
        switch curve {
        case .ed25519:
            return DIKit.resolve(tag: DIKitContext.eddsa)
        case .secp256k1:
            return DIKit.resolve(tag: DIKitContext.ecdsa)
        }
    }
}
