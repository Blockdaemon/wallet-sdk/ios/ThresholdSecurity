// Copyright © Blockdaemon All rights reserved.

import Combine
import Foundation

public enum ERSKeyPairRepositoryError: Error {
    case passwordGeneration
    case privateKeyGeneration
    case publicKeyGeneration
    case secKeyToDataFailure
    case privateKeyEncryptionFailure
    case privateKeyDecrypionFailure
}

public protocol ERSKeyPairRepositoryAPI {
    var keyPair: AnyPublisher<ERSKeyPair, ERSKeyPairRepositoryError> { get }
    func decryptPrivateKeyWithPassword(
        _ password: String,
        privateKey: Data
    ) -> AnyPublisher<Data, ERSKeyPairRepositoryError>
}
