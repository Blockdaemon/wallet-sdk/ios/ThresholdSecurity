// Copyright © Blockdaemon All rights reserved.

import Combine
import DIKit
import Errors
import Foundation

public enum RecoveryRepositoryError: Error {
    case network(NetworkError)
    case combinationError(Error)
    case finalizationError(Error)
    case tsm(Error)
    case nilDataError
    case nilPublicKey
    case emptyKeyId
    case unknown
}

public protocol RecoveryRepositoryAPI {
    func fetchRecoveryInfoForUserId(
        _ userId: String,
        sessionId: String,
        ersLabel: String,
        ersPublicKey: Data
    ) -> AnyPublisher<Data, RecoveryRepositoryError>
    
    func fetchRecoveryKeyWithRecoveryInfo(
        _ recoveryInfo: Data,
        ersPrivateKey: Data,
        ersLabel: String
    ) -> AnyPublisher<RecoveryKey, RecoveryRepositoryError>
}
