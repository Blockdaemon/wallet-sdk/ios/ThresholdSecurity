// Copyright © Blockdaemon All rights reserved.

import Foundation
import Combine
import CommonCryptoKit

public enum MessageSigningProviderError: Error {
    case tsm(Error)
}

public protocol MessageSigningProviderAPI {
    func signingRepositoryWithChainMetadata(
        _ metadata: ChainMetadata,
        keyId: String
    ) -> AnyPublisher<MessageSigningRepositoryAPI, MessageSigningProviderError>
}
