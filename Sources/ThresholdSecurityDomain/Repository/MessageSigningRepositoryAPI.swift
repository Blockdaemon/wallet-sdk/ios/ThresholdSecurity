// Copyright © Blockdaemon All rights reserved.

import Combine
import CommonCryptoKit
import DIKit
import Errors
import Foundation

public enum MessageSigningRepositoryError: Error {
    case network(NetworkError)
    case tsm(Error)
    case presignature(Error)
    case unknown
}

public protocol MessageSigningRepositoryAPI {
    func generatedSignedInputsFromTransactionHash(
        _ transactionHash: String,
        inputs: [SigningInput],
        userId: String
    ) -> AnyPublisher<[SignedInputs], MessageSigningRepositoryError>
}
