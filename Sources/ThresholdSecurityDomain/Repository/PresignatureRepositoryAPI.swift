// Copyright © Blockdaemon All rights reserved.

import Combine
import CommonCryptoKit
import DIKit
import Errors

public enum PresignatureRepositoryError: Error {
    case tsm(Error)
    case network(NetworkError)
}

public protocol PresignatureRepositoryAPI {
    func generatePresignatureSessionIdForUserId(
        _ userId: String,
        publicKey: String,
        keyId: String,
        curveType: Curve
    ) -> AnyPublisher<String, PresignatureRepositoryError>
}
