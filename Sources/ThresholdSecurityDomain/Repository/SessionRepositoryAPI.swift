// Copyright © Blockdaemon All rights reserved.

import Combine
import CommonCryptoKit
import DIKit
import Errors

public enum SessionRepositoryError: Error {
    case emptyTenantPublicKey
    case tsm(Error)
    case network(NetworkError)
}

public protocol SessionRepositoryAPI {
    func generateKeygenSessionIdForUserId(
        _ userId: String,
        curve: Curve
    ) -> AnyPublisher<String, SessionRepositoryError>
}
